from datetime import datetime
import json
import os


class OurLogger:
    def __init__(self, experiment_name="log", date_time_head=True, outputFolder=None):
        """To setup as many loggers as you want"""
        self.prefix = ""
        self._outputFolder = outputFolder
        if outputFolder is not None:
            try:
                self.prefix = outputFolder
                os.makedirs(outputFolder)

            except OSError:
                print("Creation of the directory %s failed" + outputFolder)
            else:
                print("Successfully created the directory %s" + outputFolder)

        self._nameHeader = experiment_name

        if date_time_head:
            date_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            self._nameHeader = date_time + "-" + self._nameHeader

        print("####" + experiment_name)
        print("####" + outputFolder)
        print("####" + self.prefix + self._nameHeader)
        self.logFile = open(self.prefix + self._nameHeader + ".csv", "xt")
        self.logFile2 = open(self.prefix + self._nameHeader + ".dat", "xt")     #nuovo logFile.dat
        self.commentFile = open(self.prefix + self._nameHeader + ".txt", "xt")


        self.logFileWrites = 0
        self.commentFileWrites = 0

        # formatter = logging.Formatter('%(message)s')

        # print(file_name)
        # print(log_name)

        # fileHandler = logging.FileHandler(filename=file_name, mode='w')
        # fileHandler.setFormatter(formatter)
        # streamHandler = logging.StreamHandler()
        # streamHandler.setFormatter(formatter)

        # l.setLevel(level)
        # l.addHandler(fileHandler)
        # l.addHandler(streamHandler)
        self.currentRow = {}
        self.chiavi = {}

    def getNameHeader(self):
        return self._nameHeader

    def getOutputFolder(self):
        return self._outputFolder

    def putComment(self, comment="log.log"):
        """To setup as many loggers as you want"""
        self.commentFile.write(comment + "\n")
        self.commentFileWrites = (self.commentFileWrites + 1) % 1
        if self.commentFileWrites == 0:
            self.commentFile.flush()

    def putValue(self, chiave, valore):
        if self.chiavi.get(chiave) is None:
            self.chiavi.update({chiave: len(self.chiavi)})
            self.updateKeyFile()

        self.currentRow.update({chiave: valore})

    def putChiave(self, chiave):
        if self.chiavi.get(chiave) is None:
            self.chiavi.update({chiave: len(self.chiavi)})
        self.updateKeyFile()

    def updateKeyFile(self):
        keyIndexFile = open(self.prefix + self._nameHeader + ".keys", "wt")
        json.dump(self.chiavi, keyIndexFile)
        # self.keyIndexFile.write(str(self.chiavi))
        keyIndexFile.flush()
        keyIndexFile.close()

    def newRow(self):
        toBePrinted = [0] * len(self.chiavi)
        for key in self.currentRow:
            toBePrinted[self.chiavi[key]] = self.currentRow[key]

        for i in range(len(toBePrinted)):
            self.logFile.write(str(toBePrinted[i]) + ";")
            self.logFile2.write(str(toBePrinted[i]) + "\t")    #LUIGI: nel nuovo logFile uso \t

        self.logFile.write("\n")
        self.logFile2.write("\n")
        self.currentRow = {}
        self.logFileWrites = (self.logFileWrites + 1) % 100
        if self.logFileWrites == 0:
            self.logFile.flush()
            self.logFile2.flush()

    def closeLogger(self):
        # toBePrinted = [0] * len(self.chiavi)
        # for key in self.chiavi:
        #    toBePrinted[self.chiavi[key]] = key
        # for i in range(len(toBePrinted)):
        #    self.logFile.write(str(toBePrinted[i]) + ";")
        # self.logFile.write("\n")

        self.logFile.flush()
        self.logFile.close()
        self.logFile2.flush()
        self.logFile2.close()
        self.commentFile.flush()
        self.commentFile.close()


if __name__ == "__main__":
    myLogger = OurLogger("Test01")

    myLogger.putComment("Ciao")
    myLogger.putComment("Ciao")
    myLogger.putComment("Ciao")
    myLogger.putComment("Mondo")

    myLogger.putChiave("quattro")

    myLogger.putValue("uno", 1)
    myLogger.putValue("due", 2)
    myLogger.putValue("tre", 3)
    myLogger.putValue("quattro", 4)

    myLogger.newRow()

    myLogger.putValue("uno", 5)
    myLogger.putValue("due", 6)
    myLogger.putValue("tre", 7)
    myLogger.putValue("quattro", 8)
    myLogger.putValue("due", 66)

    myLogger.newRow()

    myLogger.putComment("Bello")

    myLogger.closeLogger()
