class OurRegistry:
    def __init__(self):
        self.registry = {}
        self.chiavi = {}

    def putValue(self, chiave, valore):
        if self.chiavi.get(chiave) is None:
            self.chiavi.update({chiave: len(self.chiavi)})
            self.registry.update({chiave: []})

        self.registry[chiave].append(valore)

    def putChiave(self, chiave):
        if self.chiavi.get(chiave) is None:
            self.chiavi.update({chiave: len(self.chiavi)})

    def reset(self):
        for key in self.registry:
            self.registry[key] = []

    def hasChiave(self, chiave):
        if self.chiavi.get(chiave) is None:
            return False
        return True

if __name__ == "__main__":
    myRegistry = OurRegistry()
    myRegistry.putValue("temperatura", 10)
    myRegistry.putValue("umidità", 11)
    myRegistry.putValue("temperatura", 10)
    myRegistry.putValue("umidità", 9)
    myRegistry.putValue("temperatura", 10)
    myRegistry.putValue("umidità", 8)
    myRegistry.putValue("temperatura", 10)
    myRegistry.putValue("umidità", 10)
    myRegistry.putValue("temperatura", 10)
    myRegistry.putValue("umidità", 12)
    myRegistry.putValue("temperatura", 10)
    myRegistry.putValue("umidità", 13)

    print(str(myRegistry.registry))
    myRegistry.reset()

    myRegistry.putValue("temperatura", 8)
    myRegistry.putValue("umidità", 11)
    myRegistry.putValue("temperatura", 1)
    myRegistry.putValue("umidità", 9)
    myRegistry.putValue("temperatura", 0)
    myRegistry.putValue("umidità", 8)
    myRegistry.putValue("temperatura", 30)
    myRegistry.putValue("umidità", 10)
    myRegistry.putValue("temperatura", 40)
    myRegistry.putValue("umidità", 12)

    print(str(myRegistry.registry))
    myRegistry.reset()
