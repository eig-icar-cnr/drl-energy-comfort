
from charlie_simulation import charlieGraficamu as graficamu
if __name__ == "__main__":
    to_be_draw = [{
                      'filename': "Test-0716-WithPersonBehavior06-{'env_person_params' [1, 2, 22], 'env_reward_weights' [1.0, 0.0, 0.0, 0.0]}",
                      'label': "Test-0716-WithPersonBehavior06-{'env_person_params' [1, 2, 22], 'env_reward_weights' [1.0, 0.0, 0.0, 0.0]}"},
                  {
                      'filename': "Test-0716-WithPersonBehavior06-{'env_person_params' [1, 2, 22], 'env_reward_weights' [0.0, 0.0, 1.0, 0.0]}",
                      'label': "Test-0716-WithPersonBehavior06-{'env_person_params' [1, 2, 22], 'env_reward_weights' [0.0, 0.0, 1.0, 0.0]}"},
                  {
                      'filename': "Test-0716-WithPersonBehavior06-{'env_person_params' [1, 2, 22], 'env_reward_weights' [0.5, 0.0, 0.5, 0.0]}",
                      'label': "Test-0716-WithPersonBehavior06-{'env_person_params' [1, 2, 22], 'env_reward_weights' [0.5, 0.0, 0.5, 0.0]}"},
                  {
                      'filename': "Test-0716-WithPersonBehavior06-{'env_person_params' [1, 16, 22], 'env_reward_weights' [1.0, 0.0, 0.0, 0.0]}",
                      'label': "Test-0716-WithPersonBehavior06-{'env_person_params' [1, 16, 22], 'env_reward_weights' [1.0, 0.0, 0.0, 0.0]}"}]
    grafParams = {'titlePrefix': '', 'titleSuffix': '', 'aggrWindowLength': 2000,
                  'sourceFolder': '../_experiments/Test-0716-WithPersonBehavior06/results/',
                  'outputFolder': '../_experiments/Test-0716-WithPersonBehavior06/figures/', 'showPlot': False}

    graficamu.plotAll(to_be_draw, grafParams)