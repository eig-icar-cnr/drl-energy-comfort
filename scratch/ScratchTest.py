import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np

a = None
if a:
    print("0: True")
if not a:
    print("1: False")
if a == True:
    print("2: True")
if a == False:
    print("3: False")

colors=mcolors.TABLEAU_COLORS

a=[1,2,3,4,3,2,1]
print(colors.items())
print(colors.keys())
print(colors.values())
print(list(colors.keys()))
print(colors["tab:red"])
colorList=list(mcolors.TABLEAU_COLORS.values())
plt.plot(a, color=colorList[14%10])
#plt.plot(a, linestyle="-", linewidth=1, markersize=5, marker="o"", markevery=1,
#                 fillstyle='none', color=colors["tab:red"], label="ciao")
plt.show()

#x = np.linspace(0, 6*np.pi, 100)
#y = np.sin(x)


# plt.ion()
#
# fig = plt.figure()
# ax = fig.add_subplot(111)
# line1, = ax.plot(x, y, 'r-')
# plt.draw()
#
# for phase in np.linspace(0, 10*np.pi, 500):
#     line1.set_ydata(np.sin(x + phase))
#     plt.draw()
#     plt.pause(0.02)
#
# plt.ioff()


x = np.linspace(0, 6*np.pi, 100)
y = np.sin(x)
plt.ion()
fig = plt.figure()
ax = fig.add_subplot(111)
line1, = ax.plot(x, y, 'r-')
ax.axis([0, 10, 0, 1])
plt.draw()

for i in range(10):
    y = np.random.random()
    #y= np.sin(x + i)
    line1.set_ydata(y)
    plt.draw()
    #plt.scatter(i, y)
    plt.pause(0.05)

#plt.show()

plt.ioff()
