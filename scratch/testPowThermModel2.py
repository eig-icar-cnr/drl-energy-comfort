from charlie_simulation import PowThermModel as ptm
import matplotlib.pyplot as plt
import numpy as np

if __name__ == "__main__":
    model = ptm.PowThermModel(stepTime=10, hvacModel="30")

    simulationStepDuration = 5 * 60
    simulationTime = 60 * 60 * 3
    simulationSteps = int(simulationTime / simulationStepDuration)
    print(simulationSteps)
    indoorTemp = 10
    desiredTemp = 2
    outdoorTemp = [10] * simulationSteps

    # for i in range(0,10):
    #     outdoorTemp[i]=8
    # for i in range(10, 20):
    #     outdoorTemp[i]=8+i-10
    # for i in range(20, 40):
    #     outdoorTemp[i]=18
    # for i in range(40, 50):
    #     outdoorTemp[i]=18-i+40
    # for i in range(50, 60):
    #     outdoorTemp[i]=8-(i-50)/2

    # input
    duration = simulationStepDuration
    # outdoorTemp=3

    # output
    output = [(indoorTemp, 0, 0)]
    outIndoorTemp = []
    outEnergiaConsumata = []
    outCosto = []
    outIndoorTemp.append(indoorTemp)
    outEnergiaConsumata.append(0)
    outCosto.append(0)
    level = 0
    for i in range(0, simulationSteps):
        if indoorTemp > desiredTemp + 0.2:
            level = 0
        if indoorTemp < desiredTemp - 0.2:
            level = 3
        indoorTemp, energiaConsumata, costo = model.compute(duration, indoorTemp, outdoorTemp[i], level)
        output.append((indoorTemp, energiaConsumata, costo))
        outIndoorTemp.append(indoorTemp)
        outEnergiaConsumata.append(energiaConsumata)
        outCosto.append(costo)
        # indoorTemp,b,c=model.compute(duration,indoorTemp,outdoorTemp,2)
        # output.append(indoorTemp)

    print(output)

    error = [abs(x - y) for x, y in zip([desiredTemp] * simulationSteps, outIndoorTemp)]
    # error=outdoorTemp([desiredTemp]*simulationSteps)
    headSize = int(9 * len(error) / 10)
    tailSize = len(error) - headSize
    # print(error)
    lastError = error[headSize:]
    #  print(lastError)
    print(sum(lastError))
    print(sum(lastError) / tailSize)

    plt.title("Temperatura Indoor (°C)")
    plt.plot(outIndoorTemp)
    plt.plot(outdoorTemp)
    plt.show()
    plt.title("Energia Consumata (wattora)")
    plt.plot(outEnergiaConsumata)
    plt.show()
    plt.title("Costo (Euro)")
    plt.plot(outCosto)
    plt.show()
    plt.title("Energia Consumata Cumulata (wattora)")
    plt.plot(np.cumsum(outEnergiaConsumata))
    plt.show()
    plt.title("Costo Cumulato (Euro)")
    plt.plot(np.cumsum(outCosto))
    plt.show()

    plt.title("Tuttinsieme")
    plt.plot(output)
    plt.show()  # 25.630842547500833 # 25.64371275372928
    # 25.64371275372928
    # 25.63554357644711

    # 15.108858708121613 - 26.995165782982298    (1   secondi)
    # 15.111879820750222 - 27.007412587087025    (10  secondi)
    # 15.12887927814968  - 27.076014265106615    (60  secondi)
    # 15.215962736318408 - 27.4193104188494      (300 secondi)
