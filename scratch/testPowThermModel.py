import matplotlib.pyplot as plt
import numpy as np
from charlie_simulation import PowThermModel as ptm

if __name__ == "__main__":
    model = ptm.PowThermModel(stepTime=60)

    # input
    duration = 5 * 60
    outdoorTemp = 3.0
    indoorTemp = 18.0

    # output
    output = []
    output.append((indoorTemp, 0.0, 0.0))
    outIndoorTemp = []
    outEnergiaConsumata = []
    outCosto = []
    outIndoorTemp.append(indoorTemp)
    outEnergiaConsumata.append(0.0)
    outCosto.append(0.0)

    for i in range(0, 60):
        level = 2
        indoorTemp, energiaConsumata, costo = model.compute(duration, indoorTemp, outdoorTemp, level)
        output.append((indoorTemp, energiaConsumata, costo))
        outIndoorTemp.append(indoorTemp)
        outEnergiaConsumata.append(energiaConsumata)
        outCosto.append(costo)
        # indoorTemp,b,c=model.compute(duration,indoorTemp,outdoorTemp,2)
        # output.append(indoorTemp)

    print(output)

    plt.title("Temperatura Indoor (°C)")
    plt.plot(outIndoorTemp)
    plt.show()
    plt.title("Energia Consumata (wattora)")
    plt.plot(outEnergiaConsumata)
    plt.show()
    plt.title("Costo (Euro)")
    plt.plot(outCosto)
    plt.show()
    plt.title("Energia Consumata Cumulata (wattora)")
    plt.plot(np.cumsum(outEnergiaConsumata))
    plt.show()
    plt.title("Costo Cumulato (Euro)")
    plt.plot(np.cumsum(outCosto))
    plt.show()

    plt.title("Tuttinsieme")
    plt.plot(output)
    plt.show()  # 25.630842547500833 # 25.64371275372928
    # 25.64371275372928
    # 25.63554357644711

    # 15.108858708121613 - 26.995165782982298    (1   secondi)
    # 15.111879820750222 - 27.007412587087025    (10  secondi)
    # 15.12887927814968  - 27.076014265106615    (60  secondi)
    # 15.215962736318408 - 27.4193104188494      (300 secondi)