import sys
sys.path.append("../ourUtils")
sys.path.append("../charlie_simulation")
import charlie_simulation.charlieRunBatch as batchRunner

if __name__ == "__main__":
    batchParamsFile = "../_experiments_batchfiles/Test-ForPaper30k2.batch.params.json"
    print(str(len(sys.argv)))
    if len(sys.argv) > 1:
        batchParamsFile = sys.argv[1]
        print(sys.argv[1])
    print(str(batchParamsFile))
    batchRunner.runBatchFromFile(batchParamsFile)
