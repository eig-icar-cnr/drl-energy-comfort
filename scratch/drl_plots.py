import os

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib as mpl
# mpl.use('Qt5Agg')

def getDefaultParams():
    defaultParams = {
        "colors": mcolors.TABLEAU_COLORS,
        "colorList": list(mcolors.TABLEAU_COLORS.keys()),
        "markerList": ["+", "o", "x", "s", "v", "^", "1", "2", "3", "4"],
        "title": "Exp01-",
        "doPlotTitle": True,
        "figureSize": (7, 4),  # figureSize=(3.5, 2)
        "dot": 27,
        "hdSpan": 2,
        "aggrWindowLength": 2000,  # 2000 #2000
        "myFontSize": 5,
        "sourceFolder": "",
        "outputFolder": "./",
        "showPlot": True
    }
    return defaultParams

if __name__ == "__main__" :
    # Note that even in the OO-style, we use `.pyplot.figure` to create the figure.

    x_lin = [ 0,1,2,3,4,5,6,7,8,9 ]
    x_sq  = [x ** 2 for x in x_lin]
    x_cu  = [x ** 3 for x in x_lin]

    print(x_sq)
    fig, ax = plt.subplots()  # Create a figure and an axes.
    ax.plot(x_lin, label='linear')  # Plot some data on the axes.
    ax.plot(x_sq, label='quadratic')  # Plot more data on the axes...
    ax.plot(x_cu)  # ... and some more.
    ax.plot(27, label='cubic')
    ax.set_xlabel('x label')  # Add an x-label to the axes.
    ax.set_ylabel('y label')  # Add a y-label to the axes.
    ax.set_title("Simple Plot")  # Add a title to the axes.
    ax.legend()  # Add a legend.

    fig.show()

def preamble(toBeDraw, paramsIn):
    params = getDefaultParams()
    params.update(paramsIn)
    try:
        os.makedirs(params["outputFolder"])
    except OSError:
        print("Creation of the directory %s failed" % params["outputFolder"])
    else:
        print("Successfully created the directory %s" % params["outputFolder"])

    plt.rcParams.update({'font.size': params["myFontSize"]})
    i = 0
    to_be_draw = []
    for tbd in toBeDraw:
        to_be_draw.append({"filename": params["sourceFolder"] + tbd["filename"],
                           "label": tbd["label"], "color": params["colorList"][i % 10],
                           "marker": params["markerList"][i % 10]})
        i = i + 1
    return to_be_draw, params

def plotTemperatureAvg_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"]\
                + "Indoor Temperature - Episode Average Indoor Temperature - Moving Average on "\
                + str(params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["tempDayAvg"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"])
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Temperature (°C)')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)