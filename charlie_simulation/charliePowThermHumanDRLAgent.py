import csv
import json
import math
import random
import numpy as np
import tensorflow._api.v2.compat.v1 as tf

tf.disable_v2_behavior()


# note: the import of ourUtils is not required!
# from ourUtils import OurRegistry,OurLogger


class Model:
    def __init__(self, define_model_func, num_states, num_actions, batch_size):
        self._num_states = num_states
        self._num_actions = num_actions
        self._batch_size = batch_size

        # self._actions = None
        # the output operations
        self._states = None  # viene popolato da define_model_func
        self._logits = None  # viene popolato da define_model_func
        self._optimizer = None  # viene popolato da define_model_func
        self._var_init = None  # viene popolato da define_model_func
        self._q_s_a = None  # viene popolato da define_model_func
        # now setup the model

        define_model_func(self)

    def predict_one(self, state, sess):
        feed_dict = {self._states: state.reshape(1, self._num_states)}
        return sess.run(self._logits, feed_dict=feed_dict)

    def predict_batch(self, states, sess):
        return sess.run(self._logits, feed_dict={self._states: states})

    def train_batch(self, sess, x_batch, y_batch):
        sess.run(self._optimizer, feed_dict={self._states: x_batch, self._q_s_a: y_batch})
        return


class Memory:
    def __init__(self, max_memory):
        self._max_memory = max_memory
        self._samples = []
        self._index = 0

    def add_sample(self, sample):
        if len(self._samples) < self._max_memory:
            self._samples.append(sample)
        else:
            self._samples[self._index] = sample
            self._index = (self._index + 1) % self._max_memory
        # if len(self._samples) > self._max_memory:
        #    self._samples.pop(0)

    def sample(self, no_samples):
        if no_samples > len(self._samples):
            return random.sample(self._samples, len(self._samples))
        else:
            return random.sample(self._samples, no_samples)


class GameRunner:
    def __init__(self, sess, model, env, memory, max_eps, min_eps,
                 decay, gamma, render=True, myRegistry=None, myLogger=None):
        self.myLogger = myLogger
        self.myRegistry = myRegistry
        self._sess = sess
        self._env = env
        self._model = model
        self._memory = memory
        self._render = render
        self._max_eps = max_eps
        self._min_eps = min_eps
        self._decay = decay
        self._gamma = gamma
        self._eps = self._max_eps
        self._steps = 0
        self._tot_reward = 0
        self._numEpisodes = 0

        self._log_init()

    def _log_init(self):
        self.myLogger.putChiave("STEP")
        self.myLogger.putChiave("Total reward")
        self.myLogger.putChiave("temperatura confort")
        self.myLogger.putChiave("temperatura indoor iniziale")
        self.myLogger.putChiave("temperatura indoor finale")
        self.myLogger.putChiave("temperatura outdoor iniziale")
        self.myLogger.putChiave("temperatura outdoor finale")
        self.myLogger.putChiave("Eps")

        return

    def _log(self,exportEpisode=False):
        self.myLogger.putValue("STEP", self._steps)
        self.myLogger.putValue("Total reward", self._tot_reward)
        self.myLogger.putValue("temperatura confort", self._env.confortT)
        self.myLogger.putValue("temperatura indoor iniziale", self._env.indoorT_init)
        self.myLogger.putValue("temperatura indoor finale", self._env.indoorT)
        self.myLogger.putValue("temperatura outdoor iniziale", self._env.outdoorT_init)
        self.myLogger.putValue("temperatura outdoor finale", self._env.outdoorT)
        self.myLogger.putValue("Eps", self._eps)
        self.myLogger.putValue("episodeNumber", self._numEpisodes)

        scartoQuadrato = [((x - y) ** 2) for x, y in zip(self.myRegistry.registry["indoorT"],
                                                         [self._env.confortT] * len(
                                                             self.myRegistry.registry["indoorT"]))]
        errDay = math.sqrt(sum(scartoQuadrato) / len(scartoQuadrato))
        unOraInTimestep = int(60 * 60 / self._env.stepTime)
        errLastHour = math.sqrt(sum(scartoQuadrato[-unOraInTimestep:]) / unOraInTimestep)
        errFirstHour = math.sqrt(sum(scartoQuadrato[0:unOraInTimestep]) / unOraInTimestep)

        #if self.myRegistry.hasChiave("rewardTemp"):
        #    rewardCumulTemp = sum(self.myRegistry.registry["rewardTemp"])
        #    rewardTempAvg = sum(self.myRegistry.registry["rewardTemp"]) / len(self.myRegistry.registry["rewardTemp"])
        #    self.myLogger.putValue("rewardCumulTemp", rewardCumulTemp)
        #    self.myLogger.putValue("rewardTempAvg", rewardTempAvg)


        #if self.myRegistry.hasChiave("rewardCons"):
        #    rewardCumulCons = sum(self.myRegistry.registry["rewardCons"])
        #    rewardConsAvg = sum(self.myRegistry.registry["rewardCons"]) / len(self.myRegistry.registry["rewardCons"])
        #    self.myLogger.putValue("rewardCumulCons", rewardCumulCons)
        #    self.myLogger.putValue("rewardConsAvg", rewardConsAvg)

        energiaConsumata = sum(self.myRegistry.registry["energiaConsumata"])
        enerConsFirstHour = sum(self.myRegistry.registry["energiaConsumata"][0:unOraInTimestep])
        enerConsLastHour = sum(self.myRegistry.registry["energiaConsumata"][-unOraInTimestep:])



        #rewardAvg = sum(self.myRegistry.registry["reward"]) / len(self.myRegistry.registry["reward"])
        tempDayAvg = sum(self.myRegistry.registry["indoorT"]) / len(self.myRegistry.registry["indoorT"])
        enerConsFirstHourAvg = sum(self.myRegistry.registry["energiaConsumata"][0:unOraInTimestep]) / unOraInTimestep
        enerConsLastHourAvg = sum(self.myRegistry.registry["energiaConsumata"][-unOraInTimestep:]) / unOraInTimestep
        enerConsDayAvg = sum(self.myRegistry.registry["energiaConsumata"]) / len(
            self.myRegistry.registry["energiaConsumata"])



        self.myLogger.putValue("energiaConsumata", energiaConsumata)
        self.myLogger.putValue("errDay", errDay)
        self.myLogger.putValue("errLastHour", errLastHour)
        self.myLogger.putValue("errFirstHour", errFirstHour)
        self.myLogger.putValue("enerConsFirstHour", enerConsFirstHour)
        self.myLogger.putValue("enerConsLastHour", enerConsLastHour)



        #self.myLogger.putValue("rewardAvg", rewardAvg)
        self.myLogger.putValue("tempDayAvg", tempDayAvg)
        self.myLogger.putValue("enerConsFirstHourAvg", enerConsFirstHourAvg)
        self.myLogger.putValue("enerConsLastHourAvg", enerConsLastHourAvg)
        self.myLogger.putValue("enerConsDayAvg", enerConsDayAvg)

        #todo invocare logging da environment
        if exportEpisode >= 0:
            d=self.myRegistry.registry
            keys = sorted(d.keys())
            myrfilename=""+self.myLogger.prefix+self.myLogger._nameHeader+"_episodeSnap_"+str(exportEpisode)+".csv"
            with open(myrfilename, "w") as outfile:
                writer = csv.writer(outfile, delimiter=";")
                writer.writerow(keys)
                writer.writerows(zip(*[d[key] for key in keys]))

            #a_file = open(""+self.myLogger.prefix+self.myLogger._nameHeader+"_episodeSnap.json", "w")
            #json.dump(self.myRegistry.registry, a_file)
            #a_file.close()

        self._env.logEpisode()

        self.myRegistry.reset()
        self.myLogger.newRow()

        return

    def run(self,exportEpisode=-1):
        state = self._env.reset()
        self._tot_reward = 0
        while True:
            # if self._logging:
            #    self._log()

            if self._render:
                self._env.render()

            action = self._choose_action(state)
            next_state, reward, done, info = self._env.step(action)

            if done:
                next_state = None

            sample = (state, action, reward, next_state)
            self._memory.add_sample(sample)
            # print(str(sample))

            # exponentially decay the eps value
            self._steps += 1
            self._eps = self._min_eps + (self._max_eps - self._min_eps) * math.exp(-self._decay * self._steps)

            # move the agent to the next state and accumulate the reward
            state = next_state
            self._tot_reward += reward

            # if the game is done, break the loop
            if done:
                break

        self._numEpisodes = self._numEpisodes + 1
        self._log(exportEpisode)

        self._replay()

    def _choose_action(self, state):
        if random.random() < self._eps:
            return random.randint(0, self._model._num_actions - 1)
        else:
            return np.argmax(self._model.predict_one(state, self._sess))

    def _replay(self):
        batch = self._memory.sample(self._model._batch_size)
        #print(str(batch))
        states = np.array([val[0] for val in batch])
        #print(str(states))
        next_states = np.array([(np.zeros(self._model._num_states)
                                 if val[3] is None else val[3]) for val in batch])
        # predict Q(s,a) given the batch of states
        q_s_a = self._model.predict_batch(states, self._sess)
        # predict Q(s',a') - so that we can do gamma * max(Q(s'a')) below
        q_s_a_d = self._model.predict_batch(next_states, self._sess)
        # setup training arrays
        x = np.zeros((len(batch), self._model._num_states))
        y = np.zeros((len(batch), self._model._num_actions))
        for i, b in enumerate(batch):
            state, action, reward, next_state = b[0], b[1], b[2], b[3]
            # get the current q values for all actions in state
            current_q = q_s_a[i]
            # update the q value for action
            if next_state is None:
                # in this case, the game completed after action, so there is no max Q(s',a')
                # prediction possible
                current_q[action] = reward
            else:
                current_q[action] = reward + self._gamma * np.amax(q_s_a_d[i])
            x[i] = state
            y[i] = current_q
        self._model.train_batch(self._sess, x, y)
