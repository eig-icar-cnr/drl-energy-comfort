
class RewardOperativeRange:
    # consumption è in wattOra e quindi da sat_param si calcola max_cons come segue
    #           sat_param
    # maxCons = 20000     * stepTime / 3600  # il consumo di un hvac70 se all'interno ci sono 10 gradi alla max velocità
    # maxCons =  9950     * stepTime / 3600  # il consumo di un hvac30 se all'interno ci sono  5 gradi alla max velocità
    # maxCons =  8900     * stepTime / 3600  # il consumo di un hvac30 se all'interno ci sono 10 gradi alla max velocità
    def __init__ (self,min_temp,max_temp,id="rewardOpRange"):
        self._min_temp=min_temp
        self._max_temp=max_temp
        self.id=id
        return

    def computeRewardFromEnv(self,env):
        return self.computeReward(env.indoorT)

    def computeReward(self,indoorT):
        if indoorT < self._min_temp:
            return -1
        elif indoorT > self._max_temp:
            return -1
        else:
            return 0



