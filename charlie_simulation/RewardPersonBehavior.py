class RewardPersonBehavior:
    # consumption è in wattOra e quindi da sat_param si calcola max_cons come segue
    #           sat_param
    # maxCons = 20000     * stepTime / 3600  # il consumo di un hvac70 se all'interno ci sono 10 gradi alla max velocità
    # maxCons =  9950     * stepTime / 3600  # il consumo di un hvac30 se all'interno ci sono  5 gradi alla max velocità
    # maxCons =  8900     * stepTime / 3600  # il consumo di un hvac30 se all'interno ci sono 10 gradi alla max velocità
    def __init__ (self,intensity,id="personBehavior"):
        self.intensity=intensity
        self.id = id
        return

    # todo verificare il metodo rispetto all'environment
    def computeRewardFromEnv(self,env):
        if env.personParams[0]==0:
            return 0
        return self.computeReward(env.personDisconfort)

    def computeReward(self,personDisconfort):
        if personDisconfort==0:
            return 0
        else:
            return -self.intensity
