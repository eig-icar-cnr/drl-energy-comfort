class PowThermModel:
    def __init__(self, stepTime=60, hvacModel="40", genVariability=1, genMaxLoad=8900):
        # Coefficiente angolare curva funzionamento ventilconvettore (pow vs temperatura ambiente)
        self._a_values = {"15": -77.857, "20": -124.71, "25": -136.43, "30": -182.86, "40": -218.14, "50": -274.29,
                          "60": -317.71,
                          "70": -431.57}

        # Termine noto curva funzionamento ventilconvettore (pow vs temperatura ambiente)
        self._b_values = {"15": 5138.1, "20": 7692.9, "25": 8452.4, "30": 10838.0, "40": 12845.0, "50": 15824.0,
                          "60": 18010.0,
                          "70": 24664}

        # trasmittanza in W/(m^3 * K)
        self._u_values = {"molto elevato": 0.232, "elevato": 0.58, "buono": 1.392, "cattivo": 3.48,
                          "quasi assente": 4.64}

        # parametro correttivo dipendente dalla velocità ventola
        self._c_values = [0.0, 0.71, 0.89, 1.0]

        # parametri modello temperatura
        self._stepTime = stepTime               # secondi tra una computazione e l'altra
        self._ro = 20.0                         # 1.225 #densità dell'aria
        self._volume = 60.0                     # Volume stanza (metri cubi)
        self._calore_specifico = 1005.0         # Calore specifico dell'aria (Joule/(kg*K))
        self._u = self._u_values["cattivo"]     # trasmittanza in W/(m^3 * K)
        self._hvacModel = hvacModel             # modello hvac per questa istanza

        # Coefficiente angolare curva funzionamento ventilconvettore (pow vs temperatura ambiente)
        self._a = self._a_values[self._hvacModel]

        # Termine noto curva funzionamento ventilconvettore (pow vs temperatura ambiente)
        self._b = self._b_values[self._hvacModel]

        # parti fisse funzione modello temperatura
        self._partA = 1 / (self._ro * self._volume * self._calore_specifico)
        self._partB = self._u / (self._ro * self._calore_specifico)

        # parametri metano
        self._costo_metano = 0.80  # euro / m^3
        self._pci = 9275.0  # Potere Calorifico Inferiore WattOra / m^3

        # parametri curva rendimento di generazione
        self._x4 = -0.000004
        self._x3 = 0.0011
        self._x2 = -0.1076
        self._x = 4.1783
        self._tn = 52.0710

        # parametri modello energia
        self._eta_emissione = 0.95
        self._eta_regolazione = 0.96
        self._eta_distribuzione = 0.94
        self._eta_generazione = 0.94
        self._genVariability = genVariability
        self._genMaxLoad = genMaxLoad #Watt - potenza max carico (usato per calcolare la percentuale di carico)


    def compute(self, duration, indoorTemp, outdoorTemp, c):
        costoCumul = 0.0
        ePrimariaCumul = 0.0

        while duration > 0:
            if duration > self._stepTime:
                stepTime = self._stepTime
            else:
                stepTime = duration
            pVent = (self._a * indoorTemp + self._b) * self._c_values[c]
            indoorTemp = indoorTemp + stepTime * self._partA * pVent - stepTime * self._partB * (
                        indoorTemp - outdoorTemp)
            eUtile = pVent * stepTime / 3600

            self._pVentPerc = (pVent / self._genMaxLoad) * 100
            if self._genVariability == 1:
                self._eta_generazione = (self._x4 * pow(self._pVentPerc, 4) + self._x3 * pow(self._pVentPerc, 3) + self._x2 * pow(self._pVentPerc, 2) + self._x * self._pVentPerc + self._tn)/100
            self._eta_globale = self._eta_emissione * self._eta_regolazione * self._eta_distribuzione * self._eta_generazione  # energiaUtile / energiaPrimaria

            ePrimaria = eUtile / self._eta_globale
            costo = self._costo_metano * (ePrimaria / self._pci)
            ePrimariaCumul += ePrimaria
            costoCumul += costo
            duration = duration - stepTime

        finalIndoorTemp = indoorTemp
        energiaConsumata = ePrimariaCumul  # wattOra
        costo = costoCumul

        return finalIndoorTemp, energiaConsumata, costo
