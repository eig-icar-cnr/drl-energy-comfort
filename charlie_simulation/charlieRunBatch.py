import charlieGraficamu as graficamu
import charliePowThermEnergyMain as expRunner
import matplotlib.pyplot as plt
import os
import json
import sys


def runBatch(experimentHead="test", experimentFolder="", sharedBatchParams=None, batchParams=None,
             grafBatchParams=None):
    if sharedBatchParams is None:
        sharedBatchParams = {}

    grafParams = {
        "titlePrefix": "",
        "titleSuffix": "",
        "aggrWindowLength": 2000,  # 2000 #2000
        "sourceFolder": experimentFolder,
        "outputFolder": experimentFolder
    }

    if grafBatchParams is not None:
        if grafBatchParams == False:
            grafParams = grafBatchParams
        else:
            grafParams.update(grafBatchParams)

    if experimentFolder != "":
        try:
            os.makedirs(experimentFolder)

        except OSError:
            print("Creation of the directory %s failed" + experimentFolder)
        else:
            print("Successfully created the directory %s" + experimentFolder)

    paramsOutputDict = {
        "experimentHead": experimentHead,
        "experimentFolder": experimentFolder,
        "sharedBatchParams": sharedBatchParams,
        "batchParams": batchParams,
        "grafParams": grafParams
    }
    paramsOutputDictJson = json.dumps(paramsOutputDict, indent=4)
    paramsBatchFileOut = open(experimentFolder + experimentHead + ".batch.params.json", "wt")
    paramsBatchFileOut.write(paramsOutputDictJson)
    paramsBatchFileOut.close()

    to_be_draw = []
    experimentBatchCount=-1
    for simParams in batchParams:
        experimentBatchCount=experimentBatchCount+1
        experimentTail = "batchNumber_"+str(experimentBatchCount)

        #experimentTail = str(simParams).replace(":", "")
        experimentName = experimentHead + "-" + experimentTail
        allParams = dict(sharedBatchParams, **simParams)
        logFile = expRunner.runExperiment(allParams, experimentName, outputFolder=experimentFolder)
        to_be_draw.append({"filename": logFile, "label": experimentName})

        paramsFileOut = open(experimentFolder + experimentHead + ".plot.params", "wt")
        paramsFileOut.write("to_be_draw=" + str(to_be_draw) + "\n")
        paramsFileOut.write("grafParams=" + str(grafParams) + "\n")
        paramsFileOut.close()

    if grafParams:
        graficamu.plotAll(to_be_draw, grafParams)
        if grafParams["showPlot"] == True:
            plt.show()


def runBatchFromFile(batchFile):
    batchParamsFile = batchFile

    data = {
        "experimentHead": "Test-default",
        "experimentFolder": "./Test-default/",
    }
    # print("Alfa "+str(batchParamsFile)+"\n")

    # print("try\n")
    paramString = open(batchParamsFile, "r").read()
    # print(str(paramString)+"\n")
    paramDict = json.loads(paramString)
    # print(str(paramDict)+"\n")
    data.update(paramDict)

    # print("param file errato")

    runBatch(data["experimentHead"], data["experimentFolder"], data["sharedBatchParams"], data["batchParams"],
             data["grafBatchParams"])


if __name__ == "__main__":
    batchParamsFile = None
    # print(str(len(sys.argv)))
    if len(sys.argv) > 1:
        batchParamsFile = sys.argv[1]
        # print(sys.argv[1])
    # print(str(batchParamsFile))
    runBatchFromFile(batchParamsFile)
