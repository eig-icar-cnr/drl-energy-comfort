
class RewardConsumption:
    # consumption è in wattOra e quindi da sat_param si calcola max_cons come segue
    #           sat_param
    # maxCons = 20000     * stepTime / 3600  # il consumo di un hvac70 se all'interno ci sono 10 gradi alla max velocità
    # maxCons =  9950     * stepTime / 3600  # il consumo di un hvac30 se all'interno ci sono  5 gradi alla max velocità
    # maxCons =  8900 (W)    * stepTime / 3600  # il consumo di un hvac30 se all'interno ci sono 10 gradi alla max velocità
    def __init__ (self,sat_param,id="rewardCons"):
        self._sat_param=sat_param
        self.id = id
        return

    def computeRewardFromEnv(self,env):
        return self.computeReward(env.stepTime,env.energiaConsumata)

    def computeReward(self,stepTime,consumption):
        maxCons = self._sat_param * stepTime / 3600
        rewardCons = -min(consumption, maxCons) / maxCons
        return rewardCons



