import tensorflow._api.v2.compat.v1 as tf
import charliePowThermEnergyEnv as simulEnv
import charliePowThermHumanDRLAgent as drla
import charlieGraficamu as graficamu
from charlie_simulation import PowThermModel as ptm
from charlie_simulation import RewardConsumption
from charlie_simulation import RewardComfortSetPoint
from charlie_simulation import RewardOperativeRange
#from charlie_simulation import RewardPersonBehavior
from ourUtils import OurRegistry, OurLogger
import os


def getDefaultParams():
    defaultParams = {
        "episodes": 5000,
        "env_step_duration": 300,
        "env_episode_duration": 43200,
        "env_person_behavior_function": "tangent",
        "env_indoor_T": 15,
        "env_outdoor_T": 10,
        "env_comfort_T": 20,
        "env_variability": [5, 5, 0],
        "env_reward_weights": [0.5, 0.5],
        "env_reward_changes":[],
        "env_reward_maxes": [10, 8900],
        "env_person_params": [0,4,22],
        "env_obs_list": ["indoorT", "outdoorT", "fancoil"],
        "env_reward_computers": [
            {
                "classname" : "RewardComfortSetPoint",
                "id" : "rewardTemp",
                "sat_param" : 10
            }
            ,
            {
                "classname": "RewardConsumption",
                "id": "rewardCons",
                "sat_param": 8900
            }
            #,
            #{
            #    "classname": "RewardOperativeRange",
            #    "id": "rewardOpRange",
            #    "min_temp": 10,
            #    "max_temp": 20
            #}


        ],
        "drl_eps_episodes_decay_rate": 0.80,
        "drl_max_epsilon": 0.99,
        "drl_min_epsilon": 0.00,
        "drl_gamma": 0.8,
        "drl_batch_size": 50,
        "drl_memory": 200000,
        "ptm_hvac_model": "30",
        "ptm_gen_variability": 0,
        "ptm_gen_maxLoad": 8900,
        "ptm_step_time": 10,
        "log_time_head": True
    }
    return defaultParams


def define_model_neural_network(model):
    model._states = tf.placeholder(shape=[None, model._num_states],
                                   dtype=tf.float32)
    model._q_s_a = tf.placeholder(shape=[None, model._num_actions],
                                  dtype=tf.float32)
    # create a couple of fully connected hidden layers
    fc1 = tf.layers.dense(model._states, 50, activation=tf.nn.relu)  # 50
    fc2 = tf.layers.dense(fc1, 50, activation=tf.nn.relu)
    fc3 = tf.layers.dense(fc2, 50, activation=tf.nn.relu)
    fc4 = tf.layers.dense(fc3, 50, activation=tf.nn.relu)
    model._logits = tf.layers.dense(fc4, model._num_actions)
    loss = tf.losses.mean_squared_error(model._q_s_a, model._logits)
    model._optimizer = tf.train.AdamOptimizer().minimize(loss)
    model._var_init = tf.global_variables_initializer()


def averageData(data, step):
    k = 0
    averagedData = data
    acc = 0
    for i in range(len(data)):
        acc = acc + data[i]
        if (i + 1) % step == 0:
            averagedData[k] = acc / step
            acc = 0
            k = k + 1

    return averagedData


# test:
# 30k episodi
# weights [1.0,0.0]
# weights [0.80,0.20]
# weights [0.60,0.40]
# weights [0.5,0.5]
# weights [0.40,0.60]
# weights [0.20,0.80]
# weights [0.0,1.0]
def initRewardComputers(rewCompSpecList):
    print("Reward Computeres Init")
    rewardComputers=list()

    for spec in rewCompSpecList:
        module = __import__(spec["classname"])
        class_ = getattr(module, spec["classname"])
        params=dict(spec) #shallow copy
        del params["classname"] # remove "classname" key
        rewardComputerInstance = class_(**params)
        rewardComputers.append(rewardComputerInstance)
        print(rewardComputerInstance.id+": "+str(rewardComputerInstance))
    return rewardComputers
    print("Reward Computeres Init end")


def runExperiment(paramsIn, experimentName="Test", outputFolder=None):
    params = getDefaultParams()
    params.update(paramsIn)
    myRegistry = OurRegistry.OurRegistry()

    myLogger = OurLogger.OurLogger(experimentName, date_time_head=params["log_time_head"], outputFolder=outputFolder)
    myLogger.putComment(experimentName)
    myLogger.putComment(str(paramsIn))
    myLogger.putComment(str(params))

    ptmModel = ptm.PowThermModel(stepTime=params["ptm_step_time"], hvacModel=params["ptm_hvac_model"], genVariability=params["ptm_gen_variability"], genMaxLoad=params["ptm_gen_maxLoad"])
    rewardComputers = initRewardComputers(params["env_reward_computers"])
    env = simulEnv.Environment(indoorT=params["env_indoor_T"], outdoorT=params["env_outdoor_T"],
                               confortT=params["env_comfort_T"], stepDuration=params["env_step_duration"],
                               episodeDuration=params["env_episode_duration"],
                               personBehaviorFunction=params["env_person_behavior_function"],
                               powThermModel=ptmModel, envVariability=params["env_variability"],
                               rewardWeights=params["env_reward_weights"], rewardMaxes=params["env_reward_maxes"],
                               personParams=params["env_person_params"], rewardComputers=rewardComputers,
                               obsList=params["env_obs_list"], myRegistry=myRegistry, myLogger=myLogger)

    num_episodes = params["episodes"]  # 100000
    max_epsilon = params["drl_max_epsilon"]
    min_epsilon = params["drl_min_epsilon"]

    print((num_episodes * env.stepsInEpisode()))

    eps_episodes_decay_rate = params["drl_eps_episodes_decay_rate"]
    decay = 5 / (eps_episodes_decay_rate * num_episodes * env.stepsInEpisode())  # decay is a function of episodes
    gamma = params["drl_gamma"]
    batch_size = params["drl_batch_size"]

    num_states = env.observation_space.shape[0]
    print(str(env.observation_space.shape[0]))
    num_actions = env.action_space.n

    # model = Model(num_states, num_actions, batch_size)
    model = drla.Model(define_model_neural_network, num_states, num_actions, batch_size, )
    mem = drla.Memory(params["drl_memory"])  # drla.Memory((num_episodes*288)/20) #200000

    myLogger.putComment("env:" + str(env.__dict__))
    myLogger.putComment("ptm:" + str(ptmModel.__dict__))
    myLogger.putComment("model:" + str(model.__dict__))
    myLogger.putComment("episodes:" + str(num_episodes))
    myLogger.putComment("perc_to_steady:" + str(eps_episodes_decay_rate))

    with tf.Session() as sess:
        sess.run(model._var_init)
        gr = drla.GameRunner(sess, model, env, mem, max_epsilon, min_epsilon,
                             decay, gamma, myRegistry=myRegistry, myLogger=myLogger)
        myLogger.putComment("gr:" + str(gr.__dict__))
        cnt = 1

        while cnt <= num_episodes:

            #cambio il peso dei reward
            for changes in params["env_reward_changes"]:
                if cnt == changes["episode_number"]:
                    env.rewardWeights=changes["env_reward_weights"]
                    print ("New Weights for Environment at episode: "+str(cnt)+" weights: "+str(env.rewardWeights))

            #stampo il numero di episodi
            if cnt % 50 == 0:
                print('Episode {} of {}'.format(cnt, num_episodes))

            exportEpisode=-1
            #toBeExported=[0.0001,0.1,0.5,0.9,0.9999]
            #toBeExported=[1, 2000, 4000, 6000, 8000, 10000, 12000, 14000, 16000, 18000, 20000, 22000, 24000, 26000, 28000, 30000]
            toBeExported = [26000, 26001, 26002, 26003, 26004, 26005, 26006, 26007, 26008, 26009, 26010, 26011, 26012, 26013, 26014, 26015, 26016, 26017, 26018, 26019, 26020]

            #if cnt in [int((num_episodes*toBeExported[t])) for t in range(0,len(toBeExported))]:
            if cnt in [int((toBeExported[t])) for t in range(0, len(toBeExported))]:
                exportEpisode=cnt
            gr.run(exportEpisode)
            cnt += 1

    return myLogger.getNameHeader()


if __name__ == "__main__":
    experimentHead = "60k-consSat8900-steadyLearnTest"
    experimentFolder = "./" + experimentHead + "/"
    generalSimParameters = {"episodes": 60000}
    simParameters = [
        {"drl_eps_episodes_decay_rate": 0.40, "env_reward_weights": [0.8, 0.2]},
        {"drl_eps_episodes_decay_rate": 0.60, "env_reward_weights": [0.8, 0.2]},
        {"drl_eps_episodes_decay_rate": 0.80, "env_reward_weights": [0.8, 0.2]},
        {"drl_eps_episodes_decay_rate": 0.40, "env_reward_weights": [0.5, 0.5]},
        {"drl_eps_episodes_decay_rate": 0.60, "env_reward_weights": [0.5, 0.5]},
        {"drl_eps_episodes_decay_rate": 0.80, "env_reward_weights": [0.5, 0.5]},
        {"drl_eps_episodes_decay_rate": 0.40, "env_reward_weights": [0.2, 0.8]},
        {"drl_eps_episodes_decay_rate": 0.60, "env_reward_weights": [0.2, 0.8]},
        {"drl_eps_episodes_decay_rate": 0.80, "env_reward_weights": [0.2, 0.8]}
    ]

    grafParams = {
        "titlePrefix": "",
        "titleSuffix": "",
        "aggrWindowLength": 2000,  # 2000 #2000
        "sourceFolder": experimentFolder,
        "outputFolder": experimentFolder
    }

    if experimentFolder != "":
        try:
            os.makedirs(experimentFolder)

        except OSError:
            print("Creation of the directory %s failed" + experimentFolder)
        else:
            print("Successfully created the directory %s" + experimentFolder)

    paramsBatchFileOut = open(experimentFolder + experimentHead + ".batch.params", "wt")
    paramsBatchFileOut.write("experimentHead=" + experimentHead + "\n")
    paramsBatchFileOut.write("experimentFolder=" + experimentFolder + "\n")
    paramsBatchFileOut.write("generalSimParameters=" + str(generalSimParameters) + "\n")
    paramsBatchFileOut.write("simParameters=" + str(simParameters) + "\n")
    paramsBatchFileOut.write("grafParams=" + str(grafParams) + "\n")
    paramsBatchFileOut.close()

    to_be_draw = []

    for simParams in simParameters:
        experimentTail = str(simParams)
        experimentName = experimentHead + "-" + experimentTail
        allParams = dict(generalSimParameters, **simParams)
        logFile = runExperiment(allParams, experimentName, outputFolder=experimentFolder)
        to_be_draw.append({"filename": logFile, "label": experimentName})

    graficamu.plotAll(to_be_draw, grafParams)

    paramsFileOut = open(experimentFolder + experimentHead + ".graf.params", "wt")
    paramsFileOut.write(str(to_be_draw) + "\n")
    paramsFileOut.write(str(grafParams) + "\n")
    print(str(grafParams))
