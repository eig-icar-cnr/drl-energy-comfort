
class RewardComfortSetPoint:
    def __init__ (self,sat_param,id="rewardTemp"):
        self._sat_param=sat_param
        self.id=id
        return

    def computeRewardFromEnv(self,env):
        return self.computeReward(env.confortT,env.indoorT)


    def computeReward(self,confortT,indoorT):
        maxDeltaT = self._sat_param
        rewardTemp = -min(abs(confortT - indoorT), maxDeltaT) / maxDeltaT
        return rewardTemp

if __name__ == "__main__":

    # costruzione di un oggetto da parametro
    a = RewardComfortSetPoint(5)
    print(str(a._sat_param))
    print(a.id)

    # costruzione di un oggetto da dizionario di parametri
    para={"sat_param":11}
    b = RewardComfortSetPoint(**para)
    print(str(b._sat_param))
    print(b.id)

    # costruzione ERRATA di un oggetto da dizionario di parametri con un parametro di troppo
    para2 = {"sat_param": 11,"pippo":20}
    c = RewardComfortSetPoint(**para2) #ERRORE
    print(str(c._sat_param))

