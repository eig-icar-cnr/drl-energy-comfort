import math

import gym
import numpy as np
from gym import spaces
from charlie_simulation import PowThermModel as ptm
import random

from charlie_simulation.RewardComfortSetPoint import RewardComfortSetPoint
from charlie_simulation.RewardConsumption import RewardConsumption


class Environment(gym.Env):
    """Prototype custom environment for comfort management that follows gym interface"""
    metadata = {'render.modes': ['human']}

    def __init__(self, indoorT=20, outdoorT=30, confortT=20, stepDuration=300, episodeDuration=24 * 60 * 60, personBehaviorFunction="tangente",
                 powThermModel=None, envVariability=(5, 5, 0), rewardWeights=(0.5, 0.5), rewardMaxes=(10, 8900), personParams = (0,4,22),
                 rewardComputers=None, obsList=None,
                 myLogger=None, myRegistry=None):
        super(Environment, self).__init__()

        self.myRegistry = myRegistry
        self.myLogger = myLogger

        self.start_indoorT = indoorT
        self.start_outdoorT = outdoorT
        self.start_confort = confortT
        self.envVariability = envVariability
        self.rewardWeights = rewardWeights
        self.rewardMaxes = rewardMaxes
        self.personParams = personParams
        self.personBehaviorFunction = personBehaviorFunction


        self.indoorT = self.start_indoorT
        self.outdoorT = self.start_outdoorT
        self.indoorT_init = self.indoorT
        self.outdoorT_init = self.outdoorT

        self.fancoil = 0
        self.confortT = self.start_confort
        self.currentStep = 0
        self.stepTime = stepDuration
        self.maxSteps = int(
            episodeDuration / self.stepTime)
        self.energiaConsumata = 0
        self.costo = 0

        self.agentFancoil=0 # azione corrente sul fancoil data dall'agente

        #todo aggiungere array di behaviors come parametro, e metterci qui un bel FOR
        #print("CIAO1:" + str(self.personParams))
        if self.personParams[0] == 1:
            #print("CIAO2:" + str(self.personParams))
            self.initPersonBehavior()

        ####

        if obsList is None:
            obsList = ["indoorT", "outdoorT", "fancoil"]
        self.obsList = obsList
        # Define action and observation space
        # They must be gym.spaces objects
        # Example when using discrete actions:
        self.action_space = spaces.Discrete(4)  # 0 set OFF, 1 set low, 2 set MEDIUM, 3 set HIGH

        # observation space with a a vector of 3 values between -10 e +50,
        # the first one is the current indoor temperature,
        # the second one is the current outdoor temperature,
        # the third one is the fancoil status: 0 if OFF , 1 if LOW, 2 if MEDIUM, 3 if HIGH
        self.observation_space = spaces.Box(-10.0, +50.0, (len(obsList), 1))  # low and high are scalars, and shape is provided
        # Example for using image as input:
        # self.observation_space = spaces.Box(low=0, high=255, shape= (HEIGHT, WIDTH, N_CHANNELS), dtype = np.uint8)

        if rewardComputers is None:
            tempRewComp=RewardComfortSetPoint(rewardMaxes[0])
            consRewComp=RewardConsumption(rewardMaxes[1])
            self.rewardComputers=[tempRewComp,consRewComp]
        else:
            self.rewardComputers=rewardComputers

        if powThermModel is None:
            self.model = ptm.PowThermModel(stepTime=self.stepTime, hvacModel="30", genVariability=0)
        else:
            self.model = powThermModel

    def stepsInEpisode(self):
        return self.maxSteps

    def step(self, action):
        # Execute one time step within the environment
        self.currentStep = self.currentStep + 1
        self.agentFancoil=action
        self.actionFancoil=self.agentFancoil
        #todo aggiungere array di behaviors come parametro, e metterci qui un bel FOR
        #print("CIAO3:" + str(self.personParams))
        if self.personParams[0] == 1:
            #print("CIAO4:" + str(self.personParams))
            self.evaluatePersonBehavior()


        done = False
        self.indoorT, self.energiaConsumata, self.costo = self.model.compute(self.stepTime, self.indoorT, self.outdoorT,
                                                                             self.actionFancoil)
        self.fancoil = self.actionFancoil

        all_rewards=[0.0]*len(self.rewardComputers)
        reward=0.0
        for i in range(len(self.rewardComputers)):
            #print(str(i)+" "+str(len(self.rewardComputers)))
            #print(self.rewardComputers[i].id)
            all_rewards[i] = self.rewardComputers[i].computeRewardFromEnv(self)
            reward = reward+self.rewardWeights[i]*all_rewards[i]


        #reward, rewardTemp, rewardCons = self.rewardFunc(self.indoorT, self.confortT, self.energiaConsumata,
        #                                                 self.stepTime, self.rewardWeights, self.rewardMaxes)

        if self.indoorT > 50:
            self.indoorT = 50
            done = True
            reward = reward - 1000

        if self.indoorT < -10:
            self.indoorT = -10
            done = True
            reward = reward - 1000

        if self.currentStep > self.maxSteps:
            done = True



        obs=np.array([0.0]*len(self.obsList))
        for i in range(len(self.obsList)):
            obs[i]=getattr(self,self.obsList[i])

        #obs = np.array([self.indoorT, self.outdoorT, self.fancoil])

        self.all_rewards=all_rewards
        self.reward=reward
        self.logStep()
        return obs, reward, done, {}

    #def rewardFunc(self, inTemp, desTemp, consumption, stepTime, weights, maxes):
    #    maxDeltaT = maxes[0]
    #    rewardTemp = -min(abs(desTemp - inTemp), maxDeltaT) / maxDeltaT
    #    # consumption è in wattOra
    #    # maxCons = 20000 * stepTime / 3600  # il consumo di un hvac70 se all'interno ci sono 10 gradi alla max velocità
    #    # maxCons =  9950 * stepTime / 3600  # il consumo di un hvac30 se all'interno ci sono  5 gradi alla max velocità
    #    # maxCons =  8900 * stepTime / 3600  # il consumo di un hvac30 se all'interno ci sono 10 gradi alla max velocità
    #    maxCons = maxes[1] * stepTime / 3600
    #    rewardCons = -min(consumption, maxCons) / maxCons
    #    rewardTot = weights[0] * rewardTemp + weights[1] * rewardCons
    #    # print(rewardTot,",",rewardCons,",",rewardTemp)
    #    return rewardTot, rewardTemp, rewardCons

    def reset(self):
        # Reset the state of the environment to an initial state
        self.indoorT = self.start_indoorT + (random.randint(0, self.envVariability[0] * 2) - self.envVariability[0])
        self.outdoorT = self.start_outdoorT + (random.randint(0, self.envVariability[1] * 2) - self.envVariability[1])
        self.confortT = self.start_confort + (random.randint(0, self.envVariability[2] * 2) - self.envVariability[2])
        self.fancoil = 0

        # reset parametri per statistiche varie sulla partita
        self.indoorT_init = self.indoorT
        self.outdoorT_init = self.outdoorT
        self.currentStep = 0
        # self.reward_range = (0, 10)  # valori messi a caso, per ora

        #todo aggiungere array di behaviors come parametro, e metterci qui un bel FOR
        #print("CIAO5:" + str(self.personParams))
        if self.personParams[0] == 1:
            #print("CIAO6:" + str(self.personParams))
            self.resetPersonBehavior()

        # Set the current step to a random point within the data frame
        obs = np.array([0.0] * len(self.obsList))
        for i in range(len(self.obsList)):
            obs[i] = getattr(self, self.obsList[i])
        return np.array(obs)

    def render(self, mode='human', close=False):
        # Render the environment to the screen
        # Render the environment to the screen
        # toPrint = 'Step: {};\tindoorT: {};\toutdoor: {};\tmandata: {}; \tfancoil: {}; \tpersonAction: {};
        # \thumanAction: {};'.format(self.currentStep,round(self.indoorT,2),round(self.outdoorT,2),
        # round(self.mandataT,2),self.fancoil, self.personAction, self.humanAction)
        # toSave =  '{};in:{};out:{};ma:{};fan:{};pA:{};hA:{};'.format(self.currentStep,round(self.indoorT,2),
        # round(self.outdoorT,2),round(self.mandataT,2),self.fancoil, self.personAction, self.humanAction)
        # toPrint = 'Step:'
        # print(toPrint)

        # log all the involved parameters
        # logging.info(toSave)
        return

    def logEpisode(self):
        # todo aggiungere array di behaviors come parametro, e metterci qui un bel FOR
        #print("CIAO7:" + str(self.personParams))
        if self.personParams[0] == 1:
            #print("CIAO8:" + str(self.personParams))
            self.logEpisodePersonBehavior()
        key="reward"
        rewardCumul = sum(self.myRegistry.registry.get(key))
        rewardAvg = sum(self.myRegistry.registry.get(key)) / len(self.myRegistry.registry.get(key))
        self.myLogger.putValue((key + "Cumul"), rewardCumul)
        self.myLogger.putValue((key + "Avg"), rewardAvg)

        for i in range(len(self.rewardComputers)):
            key=self.rewardComputers[i].id
            rewardCumul = sum(self.myRegistry.registry.get(key))
            rewardAvg = sum(self.myRegistry.registry.get(key)) / len(self.myRegistry.registry.get(key))
            self.myLogger.putValue((key+"Cumul"), rewardCumul)
            self.myLogger.putValue((key+"Avg"), rewardAvg)

        return

    def logStep(self):
        self.myRegistry.putValue("indoorT", self.indoorT)
        self.myRegistry.putValue("outdoorT", self.outdoorT)
        self.myRegistry.putValue("fancoil", self.fancoil)
        self.myRegistry.putValue("reward", self.reward)
        for i in range(len(self.rewardComputers)):
            self.myRegistry.putValue(self.rewardComputers[i].id, self.all_rewards[i])

        # self.myRegistry.putValue("rewardTemp", rewardTemp)
        # self.myRegistry.putValue("rewardCons", rewardCons)
        self.myRegistry.putValue("energiaConsumata", self.energiaConsumata)
        # todo aggiungere array di behaviors come parametro, e metterci qui un bel FOR
        #print("CIAO9:" + str(self.personParams))
        if self.personParams[0]==1:
            #print("CIAO10:" + str(self.personParams))
            self.logStepPersonBehavior()

        return

    def initPersonBehavior(self):
        self.personDeltaTmin = self.personParams[1]
        self.personDeltaTmax = self.personParams[2]  # gap di temperatura tollerato (su questo si basa la sigmoide)
        self.personConfortT = self.personParams[3]
        self.personAction = 0  # 0 se nessuna azione, 1 se incremento ventilconvettore, -1 se decremento
        self.personDisconfort = 0  # 0 se comfort, 1 se discomfort
        self.personFancoil = 0
        self.personBehaviorId=""
        self.personDisconfortCounter=0
        self.personDisagreeCounter = 0
        self.personActNoneCounter=0
        self.personActIncCounter=0 # numero di volte che incremento
        self.personActDecCounter=0 # numero di volte che decremento

    def resetPersonBehavior(self):
        self.initPersonBehavior()

    def logStepPersonBehavior(self):
        self.personDisconfortCounter=self.personDisconfortCounter+self.personDisconfort
        if self.personAction == 0:
            self.personActNoneCounter=self.personActNoneCounter + 1
        elif self.personAction == +1:
            self.personActIncCounter= self.personActIncCounter +1
        elif self.personAction == -1:
            self.personActDecCounter= self.personActDecCounter +1

        self.personDisagreeCounter=self.personDisagreeCounter+self.personDisagree
        return

    def logEpisodePersonBehavior(self):
        self.myLogger.putValue(self.personBehaviorId + "personDeltaTmin", self.personDeltaTmin)
        self.myLogger.putValue(self.personBehaviorId + "personDeltaTmax", self.personDeltaTmax)
        self.myLogger.putValue(self.personBehaviorId + "personConfortT", self.personConfortT)
        self.myLogger.putValue(self.personBehaviorId + "personDisconfortCounter",self.personDisconfortCounter)
        self.myLogger.putValue(self.personBehaviorId + "personActNoneCounter",self.personActNoneCounter)
        self.myLogger.putValue(self.personBehaviorId + "personActIncCounter", self.personActIncCounter)
        self.myLogger.putValue(self.personBehaviorId + "personActDecCounter",self.personActDecCounter)
        self.myLogger.putValue(self.personBehaviorId+"personDisagreeCounter",self.personDisagreeCounter)
        return

    def evaluatePersonBehavior(self) -> None:
        # todo ricordarsi di renderlo indipendente dal timestep della simulazione, aggiungendo un timestep(in secondi)
        #  minimo per la decisione, oopure lanciare una probabilità pari al rapporto tra i timestep
        #print("CIAO11:" + str(self.personParams))
        randomValue = random.random()#(0, 100) / 100
        # pDt=1/(1+exp(-x+DTmax/2))*(1-Pmin)+Pmin
        dt = abs(self.personConfortT - self.indoorT)
        DTmin = self.personDeltaTmin
        DTmax = self.personDeltaTmax

        Pmin = 0

        #scelgo sigmoide o tangente iperbolica
        if self.personBehaviorFunction == "tangent":

            if dt>DTmax:  # dt>DTmax
                pdt = np.tanh(dt - DTmax)
            else:  # dt<=DTmax
                pdt = 0.00

        elif self.personBehaviorFunction == "sigmoid":

            pdt = 1 / (1 + math.exp(-dt + DTmax / 2) * 1 - Pmin) + Pmin

        elif self.personBehaviorFunction == "smoothclamp":

            pdt = self.smoothclamp(dt, DTmin, DTmax)
          #  print(self.personConfortT, self.indoorT, DTmax, dt, pdt)

        #if (randomValue <= pdt):
        # se pdt=0.00 mi aspetto che l'user non interviene (però ho l'1% di possibilità che
        # radomValue=0.00 e che dunque interviene), per cui tolgo l'uguaglianza
        if (randomValue < pdt):

            if (self.personConfortT > self.indoorT):  # person ha freddo
                self.personAction=1
                self.personDisconfort=1
            else:  # person ha caldo
                self.personAction=-1
                self.personDisconfort = 1
        else:
            self.personAction=0
            self.personDisconfort = 0

        self.personFancoil = self.fancoil + self.personAction
        self.personFancoil = max(0, self.personFancoil)
        self.personFancoil = min(3, self.personFancoil)

        self.myRegistry.putValue("personFancoil",self.personFancoil)
        self.myRegistry.putValue("personAction", self.personAction)
        self.myRegistry.putValue("agentFancoil", self.agentFancoil)

        self.personDisagree=0
        if self.personAction == 0:
            self.actionFancoil = self.agentFancoil
        else:
            if ((self.fancoil-self.personFancoil)>0 and (self.fancoil-self.agentFancoil)>0) or \
                ((self.fancoil - self.personFancoil) < 0 and (self.fancoil - self.agentFancoil) < 0) or \
                (self.fancoil==3 and (self.personAction)==1 and (self.agentFancoil)==3) or \
                (self.fancoil==0 and (self.personAction)==-1 and (self.agentFancoil)==0):
                self.personDisagree=0
                self.actionFancoil = self.agentFancoil
            else:
                self.personDisagree=1
                self.actionFancoil = self.personFancoil
        self.myRegistry.putValue("personDisagree", self.personDisagree)



    def smoothclamp(self,x, xmi,xmx):
        return 0.0 + (lambda t: np.where(t < 0, 0, np.where(t <= 1, 3 * t ** 2 - 2 * t ** 3, 1)))(
            (x - xmi) / (xmx - xmi))