import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import json
import math
import os
import time


def getDefaultParams():
    defaultParams = {
        "colors": mcolors.TABLEAU_COLORS,
        "colorList": list(mcolors.TABLEAU_COLORS.keys()),
        "markerList": ["+", "o", "x", "s", "v", "^", "1", "2", "3", "4"],
        "titlePrefix": "Exp01-",
        "titleSuffix": "-Comparison",
        "doPlotTitle": True,
        "figureSize": (7, 4),  # figureSize=(3.5, 2)
        "dot": 27,
        "hdSpan": 2,
        "aggrWindowLength": 2000,  # 2000 #2000
        "myFontSize": 5,
        "sourceFolder": "",
        "outputFolder": "./",
        "showPlot": True
    }
    return defaultParams


def moving_average(a, n=3):  # a: vettore, n: ampiezza finestra  usa la somma cumulata fino all'ennesimo elemento
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def strided_app(a, L, S):  # Window len = L, Stride len/stepsize = S #calcola le finestre
    nrows = ((a.size - L) // S) + 1
    n = a.strides[0]
    return np.lib.stride_tricks.as_strided(a, shape=(nrows, L), strides=(S * n, n))


def moving_percentile(a, n, p):  # a: vettore, n: ampiezza finestra, p: percentile
    return np.percentile(strided_app(a, n, 1), p, axis=-1)


def moving_stddev(a, n):  # a: vettore, n: ampiezza finestra, p: percentile
    return np.std(strided_app(a, n, 1))


def preamble(toBeDraw, paramsIn):
    params = getDefaultParams()
    params.update(paramsIn)
    try:
        os.makedirs(params["outputFolder"])
    except OSError:
        print("Creation of the directory %s failed" % params["outputFolder"])
    else:
        print("Successfully created the directory %s" % params["outputFolder"])

    plt.rcParams.update({'font.size': params["myFontSize"]})
    i = 0
    to_be_draw = []
    for tbd in toBeDraw:
        to_be_draw.append({"filename": params["sourceFolder"] + tbd["filename"],
                           "label": tbd["label"], "color": params["colorList"][i % 10],
                           "marker": params["markerList"][i % 10]})
        i = i + 1
    return to_be_draw, params


# 'tab:blue'
# 'tab:orange'
# 'tab:green'
# 'tab:red'
# 'tab:purple'
# 'tab:brown'
# 'tab:pink'
# 'tab:gray'
# 'tab:olive'
# 'tab:cyan'


def oldGraficaTutto(toBeDraw, paramsIn, numFig=None):
    params = getDefaultParams()
    params.update(paramsIn)
    titlePrefix = params["titlePrefix"]
    titleSuffix = params["titleSuffix"]
    doPlotTitle = params["doPlotTitle"]
    figureSize = params["figureSize"]
    hdSpan = params["hdSpan"]
    aggrWindowLength = params["aggrWindowLength"]
    myFontSize = params["myFontSize"]
    try:
        os.makedirs(params["outputFolder"])
    except OSError:
        print("Creation of the directory %s failed" % params["outputFolder"])
    else:
        print("Successfully created the directory %s" % params["outputFolder"])

    plt.rcParams.update({'font.size': myFontSize})
    i = 0
    to_be_draw = []
    for tbd in toBeDraw:
        to_be_draw.append({"filename": params["sourceFolder"] + tbd["filename"],
                           "label": tbd["label"], "color": params["colorList"][i % 10],
                           "marker": params["markerList"][i % 10]})
        i = i + 1

    plot_to_draw = {
        "temperature": True,
        # "temperature":False,
        "temperaturePercWindow": True,
        # "temperaturePercWindow":False,
        "temperatureAvgWindow": True,
        # "temperatureAvgWindow":False,
        "temperatureDistribution": True,
        # "temperatureDistribution":False,
        # "humanActionDistribution":True,
        "humanActionDistribution": False,
        # "humanActionAvgWindow":True,
        "humanActionAvgWindow": False,
        "rewardPercWindow": True,
        # "rewardPercWindow":False
        "temperatureDistributionLast10%": True,
        # "temperatureDistributionLast10%":False
        "energyConsumptionDistributionLast10%": True,
        "energyConsPercWindow": True,
        "rewardsAvgWindow": True,
        "errFirstHour-DistributionLast10%": True,
        "errFirstHourAverageWindow": True,
        "errLastHour-DistributionLast10%": True,
        "errLastHourAverageWindow": True,
        "errDay-DistributionLast10%": True,
        "errDayAverageWindow": True

    }

    # plot percentile temperature

    # plot HUman Action Count distribution
    if plot_to_draw["humanActionDistribution"]:
        plotTitle = titlePrefix + "HumanActionCount-Distribution" + titleSuffix
        for tbd in to_be_draw:
            keyString = open(tbd["filename"] + ".keys", "r").read()
            keysDict = json.loads(keyString)
            tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                     usecols=(keysDict["humanActionCount"]))
        plt.figure(numFig, figsize=figureSize, dpi=320, facecolor='w', edgecolor='k')
        plt.rcParams.update({'font.size': myFontSize})
        myhisttype = 'step'
        maxPercentile = None
        for tbd in to_be_draw:
            newMaxPercentile = np.percentile(tbd["data"], 98)
            if maxPercentile is None or maxPercentile < newMaxPercentile:
                maxPercentile = newMaxPercentile
        myrange = np.arange(0.0, maxPercentile, hdSpan)
        for tbd in to_be_draw:
            plt.hist(tbd["data"], bins=myrange, density=True, histtype=myhisttype, color=tbd["color"],
                     label=tbd["label"])
        if doPlotTitle:
            plt.title(plotTitle)

        plt.legend()
        plt.ylabel(u'Prob. Density')
        plt.xlabel("Human Actions")
        plt.tight_layout()
        plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
        plt.show(block=False)

    # plot average human action count
    if plot_to_draw["humanActionAvgWindow"]:
        plotTitle = titlePrefix + "HumanActioncount-MovingAverageon2000pointsWindow" + titleSuffix
        for tbd in to_be_draw:
            keyString = open(tbd["filename"] + ".keys", "r").read()
            keysDict = json.loads(keyString)
            tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                     usecols=(keysDict["humanActionCount"]))
            tbd["data1"] = moving_average(tbd["data"], aggrWindowLength)
        plt.figure(numFig, figsize=figureSize, dpi=320, facecolor='w', edgecolor='k')
        plt.rcParams.update({'font.size': myFontSize})
        for tbd in to_be_draw:
            plt.plot(tbd["data1"], linewidth=1, markersize=5, marker=tbd["marker"], markevery=10000,
                     fillstyle='none', color=tbd["color"], label=tbd["label"])
        if doPlotTitle:
            plt.title(plotTitle)
        plt.legend()
        plt.xlabel("Episodes")
        plt.ylabel("Human Actions")
        plt.tight_layout()
        plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
        plt.show(block=False)

    # plot average temperature
    if plot_to_draw["temperature"]:
        plotTitle = titlePrefix + "Temperature-" + titleSuffix
        plt.rcParams.update({'font.size': myFontSize})
        for tbd in to_be_draw:
            keyString = open(tbd["filename"] + ".keys", "r").read()
            keysDict = json.loads(keyString)
            tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                     usecols=(keysDict["temperatura indoor finale"]))
        plt.figure(numFig, figsize=figureSize, dpi=320, facecolor='w', edgecolor='k')
        plt.rcParams.update({'font.size': myFontSize})
        for tbd in to_be_draw:
            plt.plot(tbd["data"], linewidth=1, markersize=5, marker=tbd["marker"],
                     markevery=10000,
                     fillstyle='none', color=tbd["color"], label=tbd["label"])
        if doPlotTitle:
            plt.title(plotTitle)

        plt.legend()
        plt.xlabel(u'Episodes')
        plt.ylabel(u'T [°C]')
        plt.tight_layout()
        plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
        plt.show(block=False)


def plotTemperatureFinal_PercAverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"]\
                + "Indoor Temperature - Episode Final Indoor Temperature - Moving 2nd and 98th percentile on "\
                + str(params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    for tbd in to_be_draw:
        # keyFileName=tbd["filename"]+".keys"
        # keyFile = open(tbd["filename"]+".keys","r")
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)

        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["temperatura indoor finale"]))
        tbd["data1"] = moving_percentile(tbd["data"], params["aggrWindowLength"], 2)
        tbd["data2"] = moving_percentile(tbd["data"], params["aggrWindowLength"], 98)

    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"], markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"])
        plt.plot(tbd["data2"], linestyle="--", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"])
    plt.plot(params["dot"], linestyle="--", linewidth=1, color="black", label="98th percentile")
    plt.plot(params["dot"], linestyle="-", linewidth=1, color="black", label="2nd percentile")
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Temperature (°C)')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotTemperatureFinal_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params[
                    "titlePrefix"] + "Indoor Temperature - Episode Final Indoor Temperature - Moving Average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["temperatura indoor finale"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"])
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Temperature (°C)')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotTemperatureAvg_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"]\
                + "Indoor Temperature - Episode Average Indoor Temperature - Moving Average on "\
                + str(params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["tempDayAvg"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"])
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Temperature (°C)')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)

def plotTemperatureAvgError_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"]\
                + "Indoor Temperature Error - Episode Average Temperature Error abs(Ti-td) - Moving Average on "\
                + str(params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["tempDayAvg"]))
        tbd["data2"] = [abs(x-20) for x in tbd["data"]]
        tbd["value1"] = sum(tbd["data2"])/len(tbd["data2"])
        tbd["data1"] = moving_average(tbd["data2"], params["aggrWindowLength"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"]+" "+str(tbd["value1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Temperature (°C)')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)

def plotTemperatureAvgError_DistributionLast10perc(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"]\
                + "Indoor Temperature Error - Episode Average Temperature Error abs(Ti-td) - Distribution of Last 10% Episodes"\
                + params["titleSuffix"]
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["tempDayAvg"]))
        start = int(9 * len(tbd["data"]) / 10)
        tbd["data1"] = tbd["data"][start:]
        tbd["data2"] = [abs(x - 20) for x in tbd["data1"]]
        tbd["value1"] = sum(tbd["data2"]) / len(tbd["data2"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data2"], 2)
        newMaxPercentile = np.percentile(tbd["data2"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    for tbd in to_be_draw:
        plt.hist(tbd["data2"], bins=myrange, density=True, histtype=myhisttype, color=tbd["color"],
                 label=tbd["label"] + " " + str(tbd["value1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Temperature (°C)')
    plt.ylabel('Prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)

def plotTemperatureFinal_DistributionAll(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "Indoor Temperature - Episode Final Indoor Temperature - Distribution" + params[
        "titleSuffix"]
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["temperatura indoor finale"]))
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data"], 2)
        newMaxPercentile = np.percentile(tbd["data"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    for tbd in to_be_draw:
        plt.hist(tbd["data"], bins=myrange, density=True, histtype=myhisttype, color=tbd["color"],
                 label=tbd["label"])
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Temperature (°C)')
    plt.ylabel('Prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def zappa_plotReward_PercAverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] + "Cumulative Reward - Moving Average on " + str(
        params["aggrWindowLength"]) + " points Window" + params["titleSuffix"]
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["Total reward"]))
        tbd["data1"] = moving_percentile(tbd["data"], params["aggrWindowLength"], 2)
        tbd["data2"] = moving_percentile(tbd["data"], params["aggrWindowLength"], 98)

    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"])
        plt.plot(tbd["data2"], linestyle="--", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"])
    plt.plot(params["dot"], linestyle="--", linewidth=1, color="black", label="98th percentile")
    plt.plot(params["dot"], linestyle="-", linewidth=1, color="black", label="2nd percentile")
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'TotalReward')
    plt.tight_layout()
    plt.savefig("Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotTemperatureAvg_DistributionLast10perc(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"]\
                + "Indoor Temperature - Episode Average Indoor Temperature - Distribution of Last 10% Episodes"\
                + params["titleSuffix"]
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["tempDayAvg"]))
        start = int(9 * len(tbd["data"]) / 10)
        tbd["data2"] = tbd["data"][start:]
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data2"], 2)
        newMaxPercentile = np.percentile(tbd["data2"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    for tbd in to_be_draw:
        plt.hist(tbd["data2"], bins=myrange, density=True, histtype=myhisttype, color=tbd["color"],
                 label=tbd["label"])
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Temperature (°C)')
    plt.ylabel('Prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotTemperatureFinal_DistributionLast10perc(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] \
                + "Indoor Temperature - Episode Final Indoor Temperature - Distribution of Last 10% Episodes" \
                + params["titleSuffix"]
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["temperatura indoor finale"]))
        start = int(9 * len(tbd["data"]) / 10)
        tbd["data2"] = tbd["data"][start:]
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data2"], 2)
        newMaxPercentile = np.percentile(tbd["data2"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    for tbd in to_be_draw:
        plt.hist(tbd["data2"], bins=myrange, density=True, histtype=myhisttype, color=tbd["color"],
                 label=tbd["label"])
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Temperature (°C)')
    plt.ylabel('Prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotEnerConsDay_DistributionLast10perc(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] + "Energy Consumption - Episode Total - Distribution of last 10% Episodes" + \
                params["titleSuffix"]
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["energiaConsumata"]))
        start = int(9 * len(tbd["data"]) / 10)
        tbd["data1"] = tbd["data"][start:]
        tbd["value1"] = sum(tbd["data1"]) / len(tbd["data1"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data1"], 2)
        newMaxPercentile = np.percentile(tbd["data1"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    for tbd in to_be_draw:
        plt.hist(tbd["data1"], bins=myrange, density=True, histtype=myhisttype, color=tbd["color"],
                 label=tbd["label"] + " " + str(tbd["value1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Energy (W⋅h)')
    plt.ylabel('Prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotEnerConsDayAvg_DistributionLast10perc(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] \
                + "Energy Consumption - Episode average - Distribution of last 10% episodes" \
                + params["titleSuffix"]
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["enerConsDayAvg"]))
        start = int(9 * len(tbd["data"]) / 10)
        tbd["data1"] = tbd["data"][start:]
        tbd["value1"] = sum(tbd["data1"]) / len(tbd["data1"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data1"], 2)
        newMaxPercentile = np.percentile(tbd["data1"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    for tbd in to_be_draw:
        plt.hist(tbd["data1"], bins=myrange, density=True, histtype=myhisttype, color=tbd["color"],
                 label=tbd["label"] + " " + str(tbd["value1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Energy (W⋅h)')
    plt.ylabel('Prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def zappa_plotEnerConsDay_PercAverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "EnerConsDay-2ndAnd98thPercentile - Moving Average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    print("drawing: " + plotTitle)

    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["energiaConsumata"]))
        tbd["data1"] = moving_percentile(tbd["data"], params["aggrWindowLength"], 2)
        tbd["data2"] = moving_percentile(tbd["data"], params["aggrWindowLength"], 98)

    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"])
        plt.plot(tbd["data2"], linestyle="--", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"])
    plt.plot(params["dot"], linestyle="--", linewidth=1, color="black", label="98th percentile")
    plt.plot(params["dot"], linestyle="-", linewidth=1, color="black", label="2nd percentile")
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Episode Energy Comsumption (W)')
    plt.tight_layout()
    plt.savefig("Fig-" + plotTitle + ".pdf")
    plt.savefig("Fig-" + "pippo" + ".pdf", format="png")
    time.sleep(5)
    if params["showPlot"]:
        plt.show(block=False)
    time.sleep(5)


def plotRewardsComponents_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] \
                + "Reward - Episode Cumulative Rewards of Unweighted Components - Moving Average on " \
                + str(params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["dataa"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                  usecols=(keysDict["rewardCumulTemp"]))
        tbd["datab"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                  usecols=(keysDict["rewardCumulCons"]))
        tbd["data1"] = moving_average(tbd["dataa"], params["aggrWindowLength"])
        tbd["data2"] = moving_average(tbd["datab"], params["aggrWindowLength"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"])
        plt.plot(tbd["data2"], linestyle="--", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"])
    plt.plot(params["dot"], linestyle="--", linewidth=1, color="black", label="consumption reward")
    plt.plot(params["dot"], linestyle="-", linewidth=1, color="black", label="temperature reward")

    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Cumulative Reward')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotRewardsComponentsAvg_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] \
                + "Reward - Episode Average Rewards of Unweighted Components - Moving Average on " \
                + str(params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["dataa"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                  usecols=(keysDict["rewardTempAvg"]))
        tbd["datab"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                  usecols=(keysDict["rewardConsAvg"]))
        tbd["data1"] = moving_average(tbd["dataa"], params["aggrWindowLength"])
        tbd["data2"] = moving_average(tbd["datab"], params["aggrWindowLength"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"])
        plt.plot(tbd["data2"], linestyle="--", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"])
    plt.plot(params["dot"], linestyle="--", linewidth=1, color="black", label="consumption reward")
    plt.plot(params["dot"], linestyle="-", linewidth=1, color="black", label="temperature reward")

    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Average Reward')
    plt.ylim([-1.05, 0.05])
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotErrFirstHour_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] + "errFisrtHour - Moving Average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["errFirstHour"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
        scartoQuadrato = [(x ** 2) for x in tbd["data"]]
        tbd["value1"] = math.sqrt(sum(scartoQuadrato) / len(scartoQuadrato))
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"] + " " + str(tbd["value1"]))

    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'°C')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotErrFirstHour_DistributionLast10perc(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] + "errFirstHour - distribution of last 10% episodes" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["errFirstHour"]))
        start = int(9 * len(tbd["data"]) / 10)
        tbd["data1"] = tbd["data"][start:]
        scartoQuadrato = [(x ** 2) for x in tbd["data1"]]
        tbd["value1"] = math.sqrt(sum(scartoQuadrato) / len(scartoQuadrato))
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data1"], 2)
        newMaxPercentile = np.percentile(tbd["data1"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    # print(str(myrange))
    for tbd in to_be_draw:
        plt.hist(tbd["data1"], bins=myrange, density=True, histtype=myhisttype,
                 color=tbd["color"],
                 label=tbd["label"] + " " + str(tbd["value1"]))
    #    print(str(tbd["data1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'°C')
    plt.ylabel(u'prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotErrLastHour_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "errLastHour - Moving Average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["errLastHour"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
        scartoQuadrato = [(x ** 2) for x in tbd["data"]]
        tbd["value1"] = math.sqrt(sum(scartoQuadrato) / len(scartoQuadrato))
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"] + " " + str(tbd["value1"]))

    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'°C')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotErrDay_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "errDay - Moving Average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["errDay"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
        scartoQuadrato = [(x ** 2) for x in tbd["data"]]
        tbd["value1"] = math.sqrt(sum(scartoQuadrato) / len(scartoQuadrato))
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"] + " " + str(tbd["value1"]))

    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'°C')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotErrDay_DistributionLast10perc(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "errDay - distribution of last 10% episodes" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["errDay"]))
        start = int(9 * len(tbd["data"]) / 10)
        tbd["data1"] = tbd["data"][start:]
        scartoQuadrato = [(x ** 2) for x in tbd["data1"]]
        tbd["value1"] = math.sqrt(sum(scartoQuadrato) / len(scartoQuadrato))
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data1"], 2)
        newMaxPercentile = np.percentile(tbd["data1"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    # print(str(myrange))
    for tbd in to_be_draw:
        plt.hist(tbd["data1"], bins=myrange, density=True, histtype=myhisttype,
                 color=tbd["color"],
                 label=tbd["label"] + " " + str(tbd["value1"]))
        # print(str(tbd["data1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'°C')
    plt.ylabel(u'prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotErrLastHour_DistributionLast10perc(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] + "errLastHour - distribution of last 10% episodes" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["errLastHour"]))
        start = int(9 * len(tbd["data"]) / 10)
        tbd["data1"] = tbd["data"][start:]
        scartoQuadrato = [(x ** 2) for x in tbd["data1"]]
        tbd["value1"] = math.sqrt(sum(scartoQuadrato) / len(scartoQuadrato))
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data1"], 2)
        newMaxPercentile = np.percentile(tbd["data1"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    # print(str(myrange))
    for tbd in to_be_draw:
        plt.hist(tbd["data1"], bins=myrange, density=True, histtype=myhisttype,
                 color=tbd["color"],
                 label=tbd["label"] + " " + str(tbd["value1"]))
        # print(str(tbd["data1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'°C')
    plt.ylabel(u'prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotEnerConsLastHour_DistributionLast10perc(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] \
                + "Energy Consumption - Episode Average of the Last Hour - distribution of last 10% episodes" \
                + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["enerConsLastHour"]))
        start = int(9 * len(tbd["data"]) / 10)
        tbd["data1"] = tbd["data"][start:]
        tbd["value1"] = sum(tbd["data1"]) / len(tbd["data1"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data1"], 2)
        newMaxPercentile = np.percentile(tbd["data1"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    # print(str(myrange))
    for tbd in to_be_draw:
        plt.hist(tbd["data1"], bins=myrange, density=True, histtype=myhisttype,
                 color=tbd["color"],
                 label=tbd["label"] + " " + str(tbd["value1"]))
        # print(str(tbd["data1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Energy (W⋅h)')
    plt.ylabel(u'prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotEnerConsFirstHour_DistributionLast10perc(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] \
                + "Energy Consumption - Episode Average of the First Hour - distribution of last 10% episodes" \
                + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["enerConsFirstHour"]))
        start = int(9 * len(tbd["data"]) / 10)
        tbd["data1"] = tbd["data"][start:]
        tbd["value1"] = sum(tbd["data1"]) / len(tbd["data1"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data1"], 2)
        newMaxPercentile = np.percentile(tbd["data1"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    # print(str(myrange))
    for tbd in to_be_draw:
        plt.hist(tbd["data1"], bins=myrange, density=True, histtype=myhisttype,
                 color=tbd["color"],
                 label=tbd["label"] + " " + str(tbd["value1"]))
        # print(str(tbd["data1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Energy (W⋅h)')
    plt.ylabel(u'prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotEnerConsLastHour_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params[
                    "titlePrefix"] + "Energy Consumption - Episode Total of the Last Hour - Moving Average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params[
                    "titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["enerConsLastHour"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
        tbd["value1"] = sum(tbd["data"]) / len(tbd["data"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"] + " " + str(tbd["value1"]))

    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Energy (W⋅h)')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotEnerConsLastHourAvg_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params[
                    "titlePrefix"] + "Energy Consumption - Episode Average of the Last Hour - Moving Average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params[
                    "titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["enerConsLastHourAvg"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
        tbd["value1"] = sum(tbd["data"]) / len(tbd["data"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"] + " " + str(tbd["value1"]))

    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Energy (W⋅h)')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotEnerConsFirstHour_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] + "Energy Consumption - Episode Total of the First Hour - average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["enerConsFirstHour"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
        tbd["value1"] = sum(tbd["data"]) / len(tbd["data"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"] + " " + str(tbd["value1"]))

    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Energy (W⋅h)')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotEnerConsFirstHourAvg_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)

    plotTitle = params["titlePrefix"] + "Energy Consumption - Episode Average of the First Hour - average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["enerConsFirstHourAvg"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
        tbd["value1"] = sum(tbd["data"]) / len(tbd["data"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"] + " " + str(tbd["value1"]))

    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Energy (W⋅h)')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotEnerConsDay_PercAverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "Energy Consumption - Episode Total - Moving 2nd and 98th percentile on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    for tbd in to_be_draw:
        # keyFileName=tbd["filename"]+".keys"
        # keyFile = open(tbd["filename"]+".keys","r")
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)

        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["energiaConsumata"]))
        tbd["data1"] = moving_percentile(tbd["data"], params["aggrWindowLength"], 2)
        tbd["data2"] = moving_percentile(tbd["data"], params["aggrWindowLength"], 98)

    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"], markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"])
        plt.plot(tbd["data2"], linestyle="--", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"])
    plt.plot(params["dot"], linestyle="--", linewidth=1, color="black", label="98th percentile")
    plt.plot(params["dot"], linestyle="-", linewidth=1, color="black", label="2nd percentile")
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Energy (W⋅h)')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotReward_PercAverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params[
                    "titlePrefix"] + "Reward - Episode Cumulative Reward - Moving 2nd and 98th percentile on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    for tbd in to_be_draw:
        # keyFileName=tbd["filename"]+".keys"
        # keyFile = open(tbd["filename"]+".keys","r")
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)

        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["Total reward"]))
        tbd["data1"] = moving_percentile(tbd["data"], params["aggrWindowLength"], 2)
        tbd["data2"] = moving_percentile(tbd["data"], params["aggrWindowLength"], 98)

    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"], markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"])
        plt.plot(tbd["data2"], linestyle="--", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"])
    plt.plot(params["dot"], linestyle="--", linewidth=1, color="black", label="98th percentile")
    plt.plot(params["dot"], linestyle="-", linewidth=1, color="black", label="2nd percentile")
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Cumulative Reward')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotEnerConsDay_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "Energy Consumption - Episode Total - Moving Average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["energiaConsumata"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
        tbd["value1"] = sum(tbd["data"]) / len(tbd["data"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"] + " " + str(tbd["value1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Energy (W⋅h)')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotEnerConsDayAvg_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "Energy Consumption - Episode Average - Moving Average on " + str(
        params["aggrWindowLength"]) + " episodes window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["enerConsDayAvg"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
        tbd["value1"] = sum(tbd["data"]) / len(tbd["data"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"] + " " + str(tbd["value1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Energy (W⋅h)')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotReward_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "Reward - Episode Cumulative Reward - Moving Average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["Total reward"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
        tbd["value1"] = sum(tbd["data"]) / len(tbd["data"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"] + " " + "{:10.4f}".format(tbd["value1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Cumulative Reward')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotRewardAvg_AverageWindow(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "Reward - Episode Average Reward - Moving Average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["rewardAvg"]))
        tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
        tbd["value1"] = sum(tbd["data"]) / len(tbd["data"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        plt.plot(tbd["data1"], linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"] + " " + "{:10.4f}".format(tbd["value1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u'Average Reward')
    plt.ylim([-1.05, 0.05])
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotReward_DistributionLast10perc(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "Reward - Episode Cumulative Reward - distribution of last 10% episodes" + \
                params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["Total reward"]))
        start = int(9 * len(tbd["data"]) / 10)
        tbd["data1"] = tbd["data"][start:]
        tbd["value1"] = sum(tbd["data1"]) / len(tbd["data1"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data1"], 2)
        newMaxPercentile = np.percentile(tbd["data1"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    # print(str(myrange))
    for tbd in to_be_draw:
        plt.hist(tbd["data1"], bins=myrange, density=True, histtype=myhisttype,
                 color=tbd["color"],
                 label=tbd["label"] + " " + str(tbd["value1"]))
        # print(str(tbd["data1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Cumulative Reward')
    plt.ylabel(u'prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)


def plotRewardAvg_DistributionLast10perc(toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "Reward - Episode Average Reward - distribution of last 10% episodes" + params[
        "titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict["rewardAvg"]))
        start = int(9 * len(tbd["data"]) / 10)
        tbd["data1"] = tbd["data"][start:]
        tbd["value1"] = sum(tbd["data1"]) / len(tbd["data1"])
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        newMinPercentile = np.percentile(tbd["data1"], 2)
        newMaxPercentile = np.percentile(tbd["data1"], 98)
        if minPercentile is None or minPercentile > newMinPercentile:
            minPercentile = newMinPercentile
        if maxPercentile is None or maxPercentile < newMaxPercentile:
            maxPercentile = newMaxPercentile
    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    # print(str(myrange))
    for tbd in to_be_draw:
        plt.hist(tbd["data1"], bins=myrange, density=True, histtype=myhisttype,
                 color=tbd["color"],
                 label=tbd["label"] + " " + str(tbd["value1"]))
        # print(str(tbd["data1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Average Reward')
    plt.ylabel(u'prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)

def plotGeneric_AverageWindow(attrName, toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "attr " + attrName + " - Moving Average on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        if attrName in keysDict:
            tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                 usecols=(keysDict.get(attrName)))
            tbd["data1"] = moving_average(tbd["data"], params["aggrWindowLength"])
            tbd["value1"] = 0
            try:
                tbd["value1"] = sum(tbd["data"] / len(tbd["data"]))
            except Exception as e:
                print(str(e))
                print("error in computing average for plot "+attrName)


            plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"],
                 markevery=10000,
                 fillstyle='none', color=tbd["color"], label=tbd["label"] + " " + str(tbd["value1"]))

    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u''+attrName)
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)

def plotGeneric_PercAverageWindow(attrName,toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params[
                    "titlePrefix"] + "attr " + attrName + " - Moving 2nd and 98th percentile on " + str(
        params["aggrWindowLength"]) + " episodes Window" + params["titleSuffix"]
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        if attrName in keysDict:
            keyString = open(tbd["filename"] + ".keys", "r").read()
            keysDict = json.loads(keyString)

            tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                     usecols=(keysDict.get(attrName)))
            tbd["data1"] = moving_percentile(tbd["data"], params["aggrWindowLength"], 2)
            tbd["data2"] = moving_percentile(tbd["data"], params["aggrWindowLength"], 98)

            plt.plot(tbd["data1"], linestyle="-", linewidth=1, markersize=5, marker=tbd["marker"], markevery=10000,
                     fillstyle='none', color=tbd["color"], label=tbd["label"])
            plt.plot(tbd["data2"], linestyle="--", linewidth=1, markersize=5, marker=tbd["marker"],
                     markevery=10000,
                     fillstyle='none', color=tbd["color"])

    plt.plot(params["dot"], linestyle="--", linewidth=1, color="black", label="98th percentile")
    plt.plot(params["dot"], linestyle="-", linewidth=1, color="black", label="2nd percentile")
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u'Episodes')
    plt.ylabel(u''+attrName)
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)

def plotGeneric_DistributionLast10perc(attrName,toBeDraw, paramsIn, numFig=None):
    to_be_draw, params = preamble(toBeDraw, paramsIn)
    plotTitle = params["titlePrefix"] + "attr " + attrName +" - distribution of last 10% episodes" + params[
        "titleSuffix"]
    plt.rcParams.update({'font.size': params["myFontSize"]})
    plt.figure(numFig, figsize=params["figureSize"], dpi=320, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': params["myFontSize"]})
    myhisttype = 'step'
    minPercentile = None
    maxPercentile = None
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        if attrName in keysDict:
            keyString = open(tbd["filename"] + ".keys", "r").read()
            keysDict = json.loads(keyString)
            tbd["data"] = np.loadtxt(open(tbd["filename"] + ".csv", "rb"), delimiter=";",
                                     usecols=(keysDict.get(attrName)))
            start = int(9 * len(tbd["data"]) / 10)
            tbd["data1"] = tbd["data"][start:]
            tbd["value1"] = sum(tbd["data1"]) / len(tbd["data1"])

            newMinPercentile = np.percentile(tbd["data1"], 2)
            newMaxPercentile = np.percentile(tbd["data1"], 98)
            if minPercentile is None or minPercentile > newMinPercentile:
                minPercentile = newMinPercentile
            if maxPercentile is None or maxPercentile < newMaxPercentile:
                maxPercentile = newMaxPercentile

    myrange = np.arange(minPercentile, maxPercentile, (maxPercentile - minPercentile) / 100)  # 0.51
    # print(str(myrange))
    for tbd in to_be_draw:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        if attrName in keysDict:
            plt.hist(tbd["data1"], bins=myrange, density=True, histtype=myhisttype,
                     color=tbd["color"],
                     label=tbd["label"] + " " + str(tbd["value1"]))
            # print(str(tbd["data1"]))
    if params["doPlotTitle"]:
        plt.title(plotTitle)

    plt.legend()
    plt.xlabel(u''+attrName)
    plt.ylabel(u'prob. density')
    plt.tight_layout()
    plt.savefig(params["outputFolder"] + "Fig-" + plotTitle + ".pdf")
    if params["showPlot"]:
        plt.show(block=False)

def plotAll(to_be_draw, paramsIn):
    params = getDefaultParams()
    if paramsIn is not None:
        params.update(paramsIn)

    to_be_draw2, params = preamble(to_be_draw, paramsIn)
    allKeys = set()
    for tbd in to_be_draw2:
        keyString = open(tbd["filename"] + ".keys", "r").read()
        keysDict = json.loads(keyString)
        allKeys.update(keysDict.keys())

    for key in allKeys:
        try:
            plotGeneric_AverageWindow(key,to_be_draw,paramsIn)
        except Exception as e:
            print(str(e))
            print("Error while plotting moving averages for attribute "+key)

        try:
            plotGeneric_DistributionLast10perc(key,to_be_draw,paramsIn)
        except Exception as e:
            print(str(e))
            print("Error while plotting distribution of last 10% for attribute "+key)

        try:
            plotGeneric_PercAverageWindow(key,to_be_draw,paramsIn)
        except Exception as e:
            print(str(e))
            print("Error while plotting 2nd and 98th percentile for attribute "+key)

    #plotAllOld(to_be_draw,paramsIn)


def plotAllOld(to_be_draw, paramsIn):
    params = getDefaultParams()
    if paramsIn is not None:
        params.update(paramsIn)

    plotCount = 0
    plotInc = 1

    # plotEnerConsDay
    plotEnerConsDay_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotEnerConsDay_DistributionLast10perc(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotEnerConsDay_PercAverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotEnerConsDayAvg_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotEnerConsDayAvg_DistributionLast10perc(to_be_draw, params, plotCount)
    plotCount += plotInc

    # plotEnerConsFirstHour
    plotEnerConsFirstHour_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotEnerConsFirstHour_DistributionLast10perc(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotEnerConsFirstHourAvg_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc

    # plotEnerConsLastHour
    plotEnerConsLastHour_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotEnerConsLastHour_DistributionLast10perc(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotEnerConsLastHourAvg_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc

    # plotErrDay
    plotErrDay_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotErrDay_DistributionLast10perc(to_be_draw, params, plotCount)
    plotCount += plotInc

    # plotErrFirstHour
    plotErrFirstHour_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotErrFirstHour_DistributionLast10perc(to_be_draw, params, plotCount)
    plotCount += plotInc

    # plotErrLastHour
    plotErrLastHour_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotErrLastHour_DistributionLast10perc(to_be_draw, params, plotCount)
    plotCount += plotInc

    # plotReward
    plotReward_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotReward_DistributionLast10perc(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotReward_PercAverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotRewardAvg_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotRewardAvg_DistributionLast10perc(to_be_draw, params, plotCount)
    plotCount += plotInc

    # plotRewardComponents
    plotRewardsComponents_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotRewardsComponentsAvg_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc

    # plotTemperatureAvg
    plotTemperatureAvg_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotTemperatureAvg_DistributionLast10perc(to_be_draw, params, plotCount)
    plotCount += plotInc

    # plotTemperatureAvgError
    plotTemperatureAvgError_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotTemperatureAvgError_DistributionLast10perc(to_be_draw, params, plotCount)
    plotCount += plotInc

    # plotTemperatureFinal
    plotTemperatureFinal_AverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotTemperatureFinal_DistributionAll(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotTemperatureFinal_DistributionLast10perc(to_be_draw, params, plotCount)
    plotCount += plotInc
    plotTemperatureFinal_PercAverageWindow(to_be_draw, params, plotCount)
    plotCount += plotInc


if __name__ == "__main__":
    to_be_draw = [{'filename': "Test-WithPersonBehavior03-{'env_obs_list' ['indoorT', 'outdoorT', 'fancoil']}",
                   'label': "Test-WithPersonBehavior03-{'env_obs_list' ['indoorT', 'outdoorT', 'fancoil']}"}, {
                      'filename': "Test-WithPersonBehavior03-{'env_obs_list' ['indoorT', 'outdoorT', 'personDisconfort', 'fancoil']}",
                      'label': "Test-WithPersonBehavior03-{'env_obs_list' ['indoorT', 'outdoorT', 'personDisconfort', 'fancoil']}"}]
    grafParams = {'titlePrefix': '', 'titleSuffix': '', 'aggrWindowLength': 2000,
                  'sourceFolder': '../_experiments/Test-WithPersonBehavior03/results/',
                  'outputFolder': '../_experiments/Test-WithPersonBehavior03/figures/', 'showPlot': False}

    plotAll(to_be_draw, grafParams)

    # graficaTutto(to_be_draw,params)
