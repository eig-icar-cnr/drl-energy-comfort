This project aims at experimenting on Deep Reinforcement Learning for managing comfort and power saving issues in a 
building.

If you use the provided data or code, we would appreciate a citation to the papers:

- L. Scarcello, F. Cicirelli, A. Guerrieri, C. Mastroianni, G. Spezzano and A. Vinci, "[Pursuing Energy Saving and Thermal Comfort With a Human-Driven DRL Approach](https://ieeexplore.ieee.org/document/9940565)," in IEEE Transactions on Human-Machine Systems, vol. 53, no. 4, pp. 707-719, Aug. 2023, doi: 10.1109/THMS.2022.3216365.
- F. Cicirelli, A. Guerrieri, C. Mastroianni, L. Scarcello, G. Spezzano and A. Vinci, "[Balancing Energy Consumption and 
Thermal Comfort with Deep Reinforcement Learning](https://ieeexplore.ieee.org/document/9582638)," 2021 IEEE 2nd 
International Conference on Human-Machine Systems (ICHMS), 2021, pp. 1-6, doi: 10.1109/ICHMS53169.2021.9582638.


# INSTALL
Install packages listed in requirements.txt

# RUN 
python runBatch.py <batchParamFile>

# batchParamFile 
A batchParamFile specifies the variable needed for running a batch of simulation. The file is formatted in json.
A batchParamFile example is reported in the file "batchParamFile.json.example".
Here follows an instance of a batchParamFile with the explaination of each field.

    {
        "experimentHead":"Test-batch",              <-This is the header shared for each simulation run
        "experimentFolder":"./Test-batch/",         <-This is the folder in which all the simulation results will be 
                                                        reported.
        "sharedBatchParams":{                       <-Contains the parameters shared between all the simulation runs.
            "episodes":30000
            "env_reward_weights": [0.5, 0.5],
            "drl_eps_episodes_decay_rate": 0.80
        },  
        "batchParams": [                            <-Contains the parameters specific for each simulation run.
            {                                       <-Example of the parameters of a single run in the batch. The 
                                                        parameters specified here will override the parameters 
                                                        specified in the sharedBatchParams section, in case of 
                                                        conflicts.
                "env_reward_weights": [0.8, 0.2]
            },     
            {                                       <-the parameters of the second run in the batch
                "env_reward_weights": [0.2, 0.8],
                "drl_eps_episodes_decay_rate": 0.40
            }
        ],
        "grafBatchParams" : {                       <-Contains the parameters required for the result plotting. It can 
                                                        be set to "false" to avoid the generation of the plots.
            "outputFolder": "./Test-batch/",        <-The folder in which the plots will be saved
            "aggrWindowLength":2000                 <-The window size of the mobile average function exploitited in 
                                                        some plots.
        }
    }

Here follows the possible parameters for "sharedBatchParams" and each of runs' parameters specified in "batchParams". 
The shown values are the default ones.

    {                  
    
        "episodes":5000,                            <-Number of episodes for a simulation run.
        "env_step_duration":300,                    <-Duration in seconds of a simulation step.
        "env_episode_duration":43200,               <-Duration in seconds of an episode.
        "env_person_behavior_function": "tangent",  <-Probability function (choose "tangent" or "sigmoid").
        "env_indoor_T":15,                          <-Average starting indoor temperature of each episode.
        "env_outdoor_T":10,                         <-Average starting outdoor temperature of each episode.
        "env_comfort_T":20,                         <-Average starting comfort temperature of each episode.
        "env_variability":[5,5,0],                  <-Variability of starting indoor_T, outdoor_T, and comfort T. 
                                                        As instance, if the env_variability related to indoor_T is 5, 
                                                        and the average starting indoor_T is 10, for each episode the 
                                                        starting indoor_T is in the range [5,15], with uniform 
                                                        distribution.
        "env_reward_weights": [0.5, 0.5],           <-Weights for the rewards components, length and order are the same 
                                                        of the rewardComputers specified in the env_reward_computers 
                                                        section.
        "env_reward_maxes":[10,8900],               <-(Deprecated) Saturation values for comfort and consumption reward 
                                                        components. Now it can be specificated into the 
                                                        env_reward_computers section
        "env_person_params": [1,1.0,4.0,22],        <- They define the behavior of the person. The first parameter is 1 
                                                        if the person is in the system. The second and third parameters
                                                        represent the Delta Tmax e Tmin of the smoothspet function.
                                                        The third parameter is the average comfort temperature of the 
                                                        person. Both the second and the third parameter describe a sigmoid 
                                                        probability function. 
        "env_obs_list": [
            "indoorT", "outdoorT", "fancoil",
            "personDisconfort"],                    <-observed state which is a subset of the possible environmental 
                                                        variables. This list can comprehend: "indoorT", "outdoorT", 
                                                        "confortT","fancoil","personDisconfort". "personDisconfort" is 1 
                                                        if the person acted on the system.
                                                        
        "env_reward_computers": [                   <-list of reward components computer specification 
            {                                           (a list of available rewardComputers follows)
             
                "classname" : "RewardPersonBehavior", <- computes a reward which depends of the behavior of a person in 
                                                        the system.
                "id" : "rewardPerson",              <-string for identifying this RewardComputer, can be changed by user
                "intensity" : 1000                  <-weight of the negative reward given when the person operates with 
                                                        the fancoil.
            },
            {
                "classname" : 
                    "RewardComfortSetPoint",        <-computes a reward as an absolute error between confortT and 
                                                        indoorT saturated with respect to the sat_param
                "id" : "rewardTemp",                <-string for identifying this RewardComputer, can be changed by user
                "sat_param" : 10                    <-saturation parameter in °C
            },
            {
                "classname": 
                    "RewardConsumption",            <-computes a reward related to the consumption, saturated w.r.t
                                                        a consumption, in Wh, expressed in sat_param
                "id": "rewardCons",                 <-string for identifying this RewardComputer, can be changed by user
                "sat_param": 8900                   <-saturation parameter in Wh
            }
            #,
            #{
            #    "classname":                         
            #        "RewardOperativeRange",         <-computes a reward as -1 if the indoorT is outside the 
            #                                            (min_temp,max_temp), 0 otherwise                 
            #    "id": "rewardOpRange",              <-string for identifying this RewardComputer, can be changed by user
            #    "min_temp": 10,                     <-minimum operative temperature in °C
            #    "max_temp": 20                      <-maximum operative temperature in °C
            #}
        "drl_eps_episodes_decay_rate": 0.80,        <-Number between 0.0 and 1.0 identifying at which episode in the 
                                                        run we can expect to have an epsilon value <0.001, which is the 
                                                        probability of choosing a random action.
        "drl_max_epsilon":0.99,                     <-Maximum epsilon value (maximum probability of choosing a random 
                                                        action).
        "drl_min_epsilon":0.00,                     <-Minimum epsilon value (minimum probability of choosing a random
                                                        action).
        "drl_gamma":0.8,                            <-Discount factor.
        "drl_batch_size":50,                        <-Number of memory tuple exploited for each learning step.
        "drl_memory":200000,                        <-Maximum number of tuples contained in the learner memory.
        "ptm_hvac_model":"30",                      <-Hvac model, can be "15","20","25","30","40","50","60","70".
        "ptm_gen_variability": 1,                   <-If "1" eta_generazione assumes variable values; if "0" constant
        "ptm_gen_maxLoad": 8900,                    <-Maximul load used to compute the load percentage and eta generazione
        "ptm_step_time":10,                         <-Maximum step, in seconds, for the evaluation of the ptm model.
        "log_time_head":true                        <-Boolean that states if a datetime tag will be prepended to each 
                                                        logfile of each batch run.
    }  


